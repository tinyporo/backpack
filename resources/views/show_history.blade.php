@extends('vendor.backpack.base.layout')

@section('content')
    <div id="chartContainer" style="height: 370px; width: 90%;"></div>
@endsection

@section('after_scripts')
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Lịch sử lên level của item <?php echo $hoi_dap_id ?>"
                },
                axisX:{
                    title: "Thời gian",
                    valueFormatString: "YYYY-MM-DD HH:mm:ss"
                },
                axisY:{
                    title: "Level",
                },
                data: [{
                    type: "line",
                    xValueType: "dateTime",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
@endsection
