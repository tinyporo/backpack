@extends('vendor.backpack.base.layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">Xuất sql</span>
            <small>Thông tin hỏi đáp</small>
        </h1>
    </section>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <form action="{{ route('export.sql') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="ids">Danh sách id</label>
                <input type="text" class="form-control-file" name="ids" id="ids">
            </div>
            <button type="submit" id="submit" class="btn btn-primary mb-2">Gửi</button>
        </form>

    </div>
</div>
@endsection

@section('after_scripts')

@endsection
