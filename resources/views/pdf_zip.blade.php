@extends('backpack::layout')

@section('header')
    <section class="content-header">
        <h1>
            <span class="text-capitalize">Đăng bộ ảnh PDF gốc</span>
            <small>Đăng ảnh gốc từ các trang PDF để mapping với các Item.</small>
        </h1>
    </section>
@endsection

@section('content')

    <div class="row m-t-20">
        <div class="">
            <div class="col-md-8 col-md-offset-2">
                <form id="frm1" name="frm1" action="{{route('upload.pdf_zip')}}" method="post"
                      enctype="multipart/form-data" onsubmit="">
                    {{ csrf_field() }}
                    <div class="col-md-12">
                        <div class="row display-flex-wrap">
                            <div class="box col-md-12 padding-10 p-t-20">
                                <div class="form-group col-xs-12">
                                    <label>File ảnh pdf gốc: </label>
                                    <small>(.zip)(max = 2Gb)</small>
                                    <input type="file" id="fupload" name="fupload" class="form-control">
                                    <button type="submit" id="save" name="save" >Tải lên</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <p>
                </p>
            </div>
        </div>
    </div>

@endsection

@section('after_scripts')
    <script>

        // $(function(){
        //     $('#frm1').submit(function (e) {
        //         if($('#fupload').val()==''){
        //             new PNotify({
        //                 // title: 'Regular Notice',
        //                 text: "Chưa nhập file upload!",
        //                 type: "notice",
        //                 icon: false
        //             });
        //             return false;
        //         }else{
        //             new PNotify({
        //                 // title: 'Regular Notice',
        //                 text: "Vui lòng đợi để upload file!",
        //                 type: "info",
        //                 icon: false
        //             });
        //
        //             $('#save').attr('disabled','disabled');
        //         }
        //     })
        //
        // });


    </script>

@endsection
