<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
{{--<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>--}}

<li class="treeview">
    <a href="#"><i class="fa fa-database"></i> <span>Dữ liệu nhập Pdf</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
{{--        <li><a href="{{ backpack_url('article') }}"><i class="fa fa-newspaper-o"></i> <span>Câu hỏi</span></a></li>--}}
        <li><a href="{{ backpack_url('hoi_dap') }}"><i class="fa fa-question"></i> <span>Hỏi đáp</span></a></li>
        <li><a href="{{ backpack_url('pdf_book') }}"><i class="fa fa-book"></i> <span>Sách PDF</span></a></li>
    </ul>
</li>

<li class="treeview">
    <a href="#"><i class="fa fa-database"></i> <span>Dữ liệu chuẩn</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('all_post') }}"><i class="fa fa-database"></i> <span>Dữ liệu tổng</span></a></li>
        <li><a href="{{ backpack_url('level') }}"><i class="fa fa-list-ul"></i> <span>Level</span></a></li>
    </ul>
</li>
{{--<li class="treeview">--}}
    {{--<a href="#"><i class="fa fa-database"></i> <span>Tài nguyên</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
    {{--<ul class="treeview-menu">--}}
        {{--<li><a href="{{ backpack_url('book') }}"><i class="fa fa-newspaper-o"></i> <span>Sách</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('book_store_activity_log') }}"><i class="fa fa-database"></i> <span>Hành vi</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('book_store') }}"><i class="fa fa-list-ul"></i> <span>Cửa hàng sách</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('classes') }}"><i class="fa fa-list-ul"></i> <span>Lớp</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('subject') }}"><i class="fa fa-list-ul"></i> <span>Môn</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('book_category') }}"><i class="fa fa-list-ul"></i> <span>Danh mục sách</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('book_sub_category') }}"><i class="fa fa-list-ul"></i> <span>Danh mục sách cấp 2</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('province') }}"><i class="fa fa-list-ul"></i> <span>Tỉnh</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('country') }}"><i class="fa fa-list-ul"></i> <span>Thành phố</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('publisher') }}"><i class="fa fa-list-ul"></i> <span>Nhà xuất bản</span></a></li>--}}
        {{--<li><a href="{{ backpack_url('author') }}"><i class="fa fa-list-ul"></i> <span>Tác giả</span></a></li>--}}
    {{--</ul>--}}
{{--</li>--}}
<li class="treeview">
    <a href="#"><i class="fa fa-barcode"></i> <span>Quản lý mã vạch</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('book-code-print') }}"><i class="fa fa-barcode"></i> Tạo mã vạch<span></span></a></li>
        <li><a href="{{ backpack_url('file-type') }}"><i class="fa fa-wrench"></i> Kiểu tài liệu<span></span></a></li>
    </ul>
</li>
<!-- Users, Roles Permissions -->
@role('Administrator')
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Quản lý người dùng</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Người dùng</span></a></li>
        <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
        <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
@endrole

<li class="treeview">
    <a href="#"><i class="fa fa-bars"></i> <span>Công cụ</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        @role('Administrator')
            <li><a href="{{ route('get.sql') }}"><i class="fa fa-file-excel-o"></i> <span>Xuất sql</span></a></li>
            <li><a href="{{ route('get.media.format2') }}"><i class="fa fa-youtube-play"></i> <span>Xuất media format 2</span></a></li>
        @endrole
        <li><a href="{{ route('get.sql.coordinate') }}"><i class="fa fa-arrows-alt"></i> <span>Xuất tọa độ</span></a></li>
        <li><a href="{{ route('get.upload.pdf_zip') }}"><i class="fa fa-upload"></i> <span>Đăng file ảnh pdf gốc</span></a></li>
    </ul>
</li>