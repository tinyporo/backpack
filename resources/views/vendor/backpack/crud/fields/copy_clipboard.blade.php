<!-- field_type_name -->
<div @include('crud::inc.field_wrapper_attributes') >
	<label>{!! $field['label'] !!}</label>

	<input type="text" value="{{ $field['default'] }}" id="info_{{ $field['name'] }}" @include('crud::inc.field_attributes')>

	<!-- The button used to copy the text -->
	<button type="button" class="btn btn-primary" onclick="copy_{{ $field['name'] }}()">Copy text</button>

	{{-- HINT --}}
	@if (isset($field['hint']))
		<p class="help-block">{!! $field['hint'] !!}</p>
	@endif

	<script>
        function copy_{{ $field['name'] }}() {
            let id = 'info_{{ $field['name'] }}';

            var copyText = document.getElementById(id);
            copyText.select();
            document.execCommand("copy");
        }
	</script>
</div>


@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
	{{-- FIELD EXTRA CSS  --}}
	{{-- push things in the after_styles section --}}

	@push('crud_fields_styles')
		<!-- no styles -->
	@endpush


	{{-- FIELD EXTRA JS --}}
	{{-- push things in the after_scripts section --}}

	@push('crud_fields_scripts')
		<!-- no scripts -->
	@endpush
@endif

{{-- Note: most of the times you'll want to use @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields)) to only load CSS/JS once, even though there are multiple instances of it. --}}