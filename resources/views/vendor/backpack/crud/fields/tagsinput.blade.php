<!-- field_type_name -->
<div @include('crud::inc.field_wrapper_attributes') >
	<label>{!! $field['label'] !!}</label>

    <input type="text" name="{{ $field['name'] }}" value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}" data-role="tagsinput" @include('crud::inc.field_attributes')>

    {{-- HINT --}}
	@if (isset($field['hint']))
		<p class="help-block">{!! $field['hint'] !!}</p>
	@endif

</div>


@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))
	{{-- FIELD EXTRA CSS  --}}
	{{-- push things in the after_styles section --}}

	@push('crud_fields_styles')
		<link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap-tagsinput.css')}}"></link>
		{{--<link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap-tagsinput-typeahead.css')}}"></link>--}}
	@endpush


	{{-- FIELD EXTRA JS --}}
	{{-- push things in the after_scripts section --}}

	@push('crud_fields_scripts')
		<script src="{{ url('/js/bootstrap-tagsinput.min.js')}}"></script>
		<script src="{{ url('/js/typeahead.bundle.min.js') }}" type="text/javascript"></script>
		<script src="{{ url('/js/bloodhound.js') }}" type="text/javascript"></script>
	@endpush
@endif

{{-- Note: most of the times you'll want to use @if ($crud->checkIfFieldIsFirstOfItsType($field, $fields)) to only load CSS/JS once, even though there are multiple instances of it. --}}