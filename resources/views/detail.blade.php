@extends('layouts.app')
@section('content')
<div>
    <div class="container">
        <div class="row detail-content">
            <div class="card-body">
                <h2>Câu hỏi</h2>
                    {!! $article->title !!}
                <h2>Đáp án</h2>
                    {!! $article->content !!}
            </div>
        </div>
    </div>
</div>

    @endsection

@section('after-scripts')
    {{--<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML' async></script>--}}
    {{--<script src="{{ url('frontend/js/detail.post.js') }}"></script>--}}
@endsection