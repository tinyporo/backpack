<?php

return [
    'image_server' => env('IMAGE_SERVER', 'http://dev.data.giaingay.io/TestProject/public/api/v1/upload_multipart'),
    'pdf_server' => env('PDF_SERVER', 'http://dev.data.giaingay.io/topica_crawler/public/api/upload_pdf'),
    'main_server' => env('MAIN_SERVER', 'http://dev.data.giaingay.io/TestProject/public/')
];
