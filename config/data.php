<?php

return [
    'table_name' => [
        'loigiaihay' => 'lgh_posts',
        'vietjack' => 'vj_posts',
        'vj_extra' => 'vjex_posts',
        'pdf' => 'pdf_posts',
        'big_school' => 'bs_posts',
    ],
    'domain' => [
        'loigiaihay' => 'https://loigiaihay.com/',
        'vietjack' => 'https://vietjack.com/',
        'pdf' => 'http://dev.data.giaingay.io/',
        'big_school' => 'https://stc.bigschool.vn/',
    ],
    'mysqldump_path' => env('MYSQLDUMP_PATH', 'mysqldump'),
];