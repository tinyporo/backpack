<?php

namespace App\Console\Commands;

use App\Models\AllPost;
use Illuminate\Console\Command;

class UpdateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $posts = AllPost::get();

//        $posts = \DB::table('all_posts')->get();
        foreach ($posts as $post){
            $post->de_bai = $this->standard($post->de_bai);
            $post->dap_an = $this->standard($post->dap_an);
            $post->save();
//            \DB::table('data')
//                ->where('hoi_dap_id', $post->hoi_dap_id)
//                ->update([
//                    'data' => $post->de_bai,
//                    'content' => $post->dap_an
//                ]);
        }
    }

    public function standard($content){
        $content = str_replace("\r", ' ', $content);
        $content = str_replace("\t", ' ', $content);
        $content = str_replace(' ', ' ', $content);
        $content = str_replace('
', ' ', $content);
//        $content = str_replace("\n", ' ', $content);
        $content = str_replace("\xc2\xa0", ' ', $content);
        $content = str_replace("&#13;", ' ', $content);

        //loại text thừa : đề bài, câu hỏi, nhãn, bình luận

        $remove_subject_texts = [
            'loigiaihay.com',
            'loigiaihay..com',
            'loigiaihay',
            'vietjack.com',
            'vietjack',
            'đề bài',
            'Đề bài',
            'câu hỏi',
            'bình luận',
        ];

        $content = str_ireplace($remove_subject_texts, '', $content);


        // loại text thừa lời giải
        $remove_texts = [
            'Giải',
            'Gỉải',
            'Giải',
            'Lời giải chi tiết',
            'Lời giải',
            'Hướng dẫn giải',
            'GỢI Ý LÀM BÀI',
            'Trả lời',
            'Phương pháp giải - Xem chi tiết',
            'Hướng dẫn giải',
            'Hướng dẫn',
            'Đáp án chi tiết',
            'Đáp án',
            'BÀI THAM KHẢO',
            'Bài Tham Khảo',
            'Hướng dẫn trả lời'
        ];

        foreach ($remove_texts as $remove_text){
            $content = preg_replace('/(<strong[^>]*>|^)\s*'.$remove_text.'\s*:?\s*<\/strong>/ui', '', $content);
            $content = preg_replace('/(<b[^>]*>|^)\s*'.$remove_text.'\s*:?\s*<\/b>/ui', '', $content);
        }

        //loại javascript
        $content = preg_replace('/<script[^>]*>.*?<\/script>/', '', $content);

        //chuyển số mũ thành dạng latext

        if(preg_match_all('/(?<=\s)([^\s>]+)\s*<sup\>([^<]+)<\/sup>/', $content, $matches)){
            foreach ($matches[0] as $k => $value){
                $co_so = $matches[1][$k];
                $he_so = $matches[2][$k];

                if($this->isValidSomu($co_so) && $this->isValidSomu($he_so)){
                    $latex = "\($co_so^$he_so\)";
                    $content = str_replace($value, $latex, $content);
                }
            }
        }

        if(preg_match_all('/(?<=\s)([^\s>]+)\s*<sub\>([^<]+)<\/sub>/', $content, $matches)){
            foreach ($matches[0] as $k => $value){
                $co_so = $matches[1][$k];
                $he_so = $matches[2][$k];

                if($this->isValidSomu($co_so) && $this->isValidSomu($he_so)){
                    $latex = "\($co_so"."_$he_so\)";
                    $content = str_replace($value, $latex, $content);
                }
            }
        }

        $end_block_tags = [
            '</p>',
            '</h1>',
            '</h2>',
            '</h3>',
            '</h4>',
            '</h5>',
            '</h6>',
            '</ol>',
            '</ul>',
            '</pre>',
            '</address>',
            '</blockquote>',
            '</dl>',
            '</div>',
            '</fieldset>',
            '</form>',
            '</hr>',
            '</noscript>',
            '</table>',
            '<br>',
            '<br/>',
        ];

        //loại tag text
        if(preg_match_all('/<[^>]*>/', $content, $matches)){

            foreach ($matches[0] as $tag_html){
                if(!preg_match('/<\s*img/', $tag_html)
                    && !preg_match('/<\s*table/', $tag_html)
                    && !preg_match('/<\/\s*table/', $tag_html)){

                    if(in_array($tag_html, $end_block_tags)) $content = str_replace($tag_html, '\n', $content);
                    else $content = str_replace($tag_html, ' ', $content);
                }
            }
        }

        //loại text thừa đầu câu
        $remove_texts2 = [
            'Lời giải chi tiết',
            'Lời giải',
            'Hướng dẫn giải',
            'GỢI Ý LÀM BÀI',
            'Trả lời',
            'Phương pháp giải - Xem chi tiết',
            'Hướng dẫn giải',
            'Hướng dẫn',
            'Đáp án chi tiết',
            'Đáp án',
            'BÀI THAM KHẢO',
            'Bài Tham Khảo',
            'Hướng dẫn trả lời'
        ];

        foreach ($remove_texts2 as $remove_text){
            $content = preg_replace('/^\s*'.$remove_text.'\s*:?\s*/ui', '', $content);
        }

        $content = preg_replace('/^\s*giải\s*:?\s*\\\n\s*/ui', '', $content);

        $content = htmlspecialchars_decode($content);
        $content = preg_replace('/(\s*\\\n\s*){2,}/', ' \n ', $content);
        $content = preg_replace("/\s{2,}/", ' ', $content);
        $content = str_ireplace("&nbsp;", ' ', $content);

        //loại \n đầu câu
        while (true){
            $content = trim($content);

            if(mb_strpos($content, '\n') === 0) $content = mb_substr($content, 2);
            else break;
        }

        //loại \n cuối câu
        while (true){
            $content = trim($content);

            if(mb_strrpos($content, '\n') === mb_strlen($content) - 2) $content = mb_substr($content, 0, mb_strlen($content) - 2);
            else break;
        }

        $content = trim($content);

        return $content;
    }
}
