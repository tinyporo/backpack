<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Markdownify\ConverterExtra;

class GetData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:data
    {--site=loigiaihay : chọn site để crawl}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $domain;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const BRAND_NAME = 'giaingay.io';

    const IMAGE_SERVER = 'dev.data.giaingay.io';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $site = $this->option('site');

        $table_name = config("data.table_name.$site");

        $posts = \DB::table($table_name)->where('is_handled', 1)
            ->where('got_data', 0)->get();
        $this->domain = config("data.domain.$site");

        foreach ($posts as $post){
            $data = $post->subject_html;
            $content = $post->content_html;

            $data = $this->handleData($data, $post, 'Problems');
            $content = $this->handleData($content, $post, 'Solutions');

            if(\Db::table('data')
                    ->where('hoi_dap_id', $post->hoi_dap_id)->count() === 0){
                \Db::table('data')
                    ->insert([
                        'post_id' => $post->id,
                        'url' => $post->url,
                        'crawler' => $post->crawler,
                        'data' => $data,
                        'content' => $content,
                        'count' => $post->count,
                        'hoi_dap_id' => $post->hoi_dap_id
                    ]);
            }else{
                \Db::table('data')
                    ->where('hoi_dap_id', $post->hoi_dap_id)
                    ->update([
                        'data' => $data,
                        'content' => $content
                    ]);
            }

            \DB::table($table_name)->where('hoi_dap_id', $post->hoi_dap_id)
                ->update([
                    'got_data' => 1
                ]);

        }
    }

    private function handleData($content, $post, $type){
        $content = str_replace("\r", ' ', $content);
        $content = str_replace("\t", ' ', $content);
        $content = str_replace(' ', ' ', $content);
        $content = str_replace('
', ' ', $content);
//        $content = str_replace("\n", ' ', $content);
        $content = str_replace("\xc2\xa0", ' ', $content);
        $content = str_replace("&#13;", ' ', $content);

        //download ảnh
        if(preg_match_all('/<img[^>]*>/', $content, $matches)){

            foreach ($matches[0] as $img_html){
                $new_html = '';

                if(preg_match('/src="[^"]+"/', $img_html, $matches)){
                    $src_html = $matches[0];


                    if(preg_match('/(?<=src=").*(?=")/', $src_html, $matches)){
                        $src = $matches[0];
                        $src = $this->convertLink($src);

                        if(mb_stripos($src, 'base64') !== false) $new_html = '';
                        else{
                            $new_src = $this->download($src, $type, $post->hoi_dap_id);

                            $new_html = '<img src="'.$new_src.'" />';

                        }
                    }
                }

                $main_server = config('crawl.main_server');

                //loại bỏ domain trong src ảnh
                $new_html = str_ireplace($main_server.'media/', 'media/', $new_html);

                $content = str_replace($img_html, $new_html, $content);

            }
        }

        //loại text thừa : đề bài, câu hỏi, nhãn, bình luận

        $remove_subject_texts = [
            'loigiaihay.com',
            'loigiaihay..com',
            'loigiaihay',
            'vietjack.com',
            'vietjack',
            'đề bài',
            'Đề bài',
            'câu hỏi',
            'bình luận',
        ];

        $content = str_ireplace($remove_subject_texts, '', $content);


        // loại text thừa lời giải
        $remove_texts = [
            'Giải',
            'Gỉải',
            'Giải',
            'Lời giải chi tiết',
            'Lời giải',
            'Hướng dẫn giải',
            'GỢI Ý LÀM BÀI',
            'Trả lời',
            'Phương pháp giải - Xem chi tiết',
            'Hướng dẫn giải',
            'Hướng dẫn',
            'Đáp án chi tiết',
            'Đáp án',
            'BÀI THAM KHẢO',
            'Bài Tham Khảo',
            'Hướng dẫn trả lời'
        ];

        foreach ($remove_texts as $remove_text){
            $content = preg_replace('/(<strong[^>]*>|^)\s*'.$remove_text.'\s*:?\s*<\/strong>/ui', '', $content);
            $content = preg_replace('/(<b[^>]*>|^)\s*'.$remove_text.'\s*:?\s*<\/b>/ui', '', $content);
        }

        //loại javascript
        $content = preg_replace('/<script[^>]*>.*?<\/script>/', '', $content);

        //chuyển số mũ thành dạng latext

        if(preg_match_all('/(?<=\s)([^\s>]+)\s*<sup\>([^<]+)<\/sup>/', $content, $matches)){
            foreach ($matches[0] as $k => $value){
                $co_so = $matches[1][$k];
                $he_so = $matches[2][$k];

                if($this->isValidSomu($co_so) && $this->isValidSomu($he_so)){
                    $latex = "\($co_so^$he_so\)";
                    $content = str_replace($value, $latex, $content);
                }
            }
        }

        if(preg_match_all('/(?<=\s)([^\s>]+)\s*<sub\>([^<]+)<\/sub>/', $content, $matches)){
            foreach ($matches[0] as $k => $value){
                $co_so = $matches[1][$k];
                $he_so = $matches[2][$k];

                if($this->isValidSomu($co_so) && $this->isValidSomu($he_so)){
                    $latex = "\($co_so"."_$he_so\)";
                    $content = str_replace($value, $latex, $content);
                }
            }
        }

        $end_block_tags = [
            '</p>',
            '</h1>',
            '</h2>',
            '</h3>',
            '</h4>',
            '</h5>',
            '</h6>',
            '</ol>',
            '</ul>',
            '</pre>',
            '</address>',
            '</blockquote>',
            '</dl>',
            '</div>',
            '</fieldset>',
            '</form>',
            '</hr>',
            '</noscript>',
            '</table>',
            '<br>',
            '<br/>',
        ];

        //chuyển table thành <table>markdown</table>
        if(preg_match_all('/<table[^>]*>.+?<\/table>/', $content, $matches)){
            foreach ($matches[0] as $k => $value){
                $origin_value = $value;

                $converter = new ConverterExtra();

                $table_tags = [
                    '<table>',
                    '</table>',
                    '<th>',
                    '</th>',
                    '<td>',
                    '</td>',
                    '<tr>',
                    '</tr>',
//                    'tbody'
                ];

                $value = str_replace("\n", '', $value);

                //xóa attribute và loại các thẻ không phải bảng
                if(preg_match_all('/(<\s*[^\s>]+)[^>]*>/', $value, $matches)){
                    foreach ($matches[0] as $j => $tag_html){
                        $replace = $matches[1][$j].'>';

                        if($replace === '<img>') continue;

                        if(in_array($replace, $table_tags)) $value = str_replace($tag_html, $replace, $value);
                        else {
                            if(in_array($tag_html, $end_block_tags)) $value = str_replace($tag_html, '\n', $value);
                            else $value = str_replace($tag_html, '', $value);
                        }
                    }
                }

                //đảm bảo tr đầu tiên gồm toàn th
                if(preg_match('/<tr\>.*?<\/tr>/', $value, $matches)){
                    $first_tr_html = $matches[0];

                    $replace_first_tr_html = str_replace('<td>', '<th>', $first_tr_html);
                    $replace_first_tr_html = str_replace('</td>', '</th>', $replace_first_tr_html);

                    $value = str_replace($first_tr_html, $replace_first_tr_html, $value);
                }

                $markdown_table = '<table>'.$converter->parseString($value).'</table>';

                $content = str_replace($origin_value, $markdown_table, $content);

            }
        }

        //loại tag text
        if(preg_match_all('/<[^<>]*>/', $content, $matches)){

            foreach ($matches[0] as $tag_html){
                if(!preg_match('/<\s*img/', $tag_html)
                    && !preg_match('/<\s*table/', $tag_html)
                    && !preg_match('/<\/\s*table/', $tag_html)){

                    if(in_array($tag_html, $end_block_tags)) $content = str_replace($tag_html, '\n', $content);
                    else $content = str_replace($tag_html, ' ', $content);
                }
            }
        }

        //loại text thừa đầu câu
        $remove_texts2 = [
            'Lời giải chi tiết',
            'Lời giải',
            'Hướng dẫn giải',
            'GỢI Ý LÀM BÀI',
            'Trả lời',
            'Phương pháp giải - Xem chi tiết',
            'Hướng dẫn giải',
            'Hướng dẫn',
            'Đáp án chi tiết',
            'Đáp án',
            'BÀI THAM KHẢO',
            'Bài Tham Khảo',
            'Hướng dẫn trả lời'
        ];

        foreach ($remove_texts2 as $remove_text){
            $content = preg_replace('/^\s*'.$remove_text.'\s*:?\s*/ui', '', $content);
        }

        $content = preg_replace('/^\s*giải\s*:?\s*\\\n\s*/ui', '', $content);

        $content = htmlspecialchars_decode($content);
        $content = preg_replace('/(\s*\\\n\s*){2,}/', ' \n ', $content);
        $content = preg_replace("/\s{2,}/", ' ', $content);
        $content = str_ireplace("&nbsp;", ' ', $content);

        //loại \n đầu câu
        while (true){
            $content = trim($content);

            if(mb_strpos($content, '\n') === 0) $content = mb_substr($content, 2);
            else break;
        }

        //loại \n cuối câu
        while (true){
            $content = trim($content);

            if(mb_strrpos($content, '\n') === mb_strlen($content) - 2) $content = mb_substr($content, 0, mb_strlen($content) - 2);
            else break;
        }

        //thay thế các đoạn link nguồn bằng tên thương hiệu ecokid
        $content = $this->standardContent($content);
        $content = trim($content);

        return $content;
    }

    private function standardContent($content){
        if(preg_match_all('/[^\s]*\.vn/', $content, $matches)){
            foreach($matches[0] as $match){
                if(!preg_match('/'.self::IMAGE_SERVER.'/', $match)){
                    $content = str_replace($match, self::BRAND_NAME, $content);
                }
            }
        }
        if(preg_match_all('/[^\s]*\.com/', $content, $matches)){
            foreach($matches[0] as $match){
                if(!preg_match('/'.self::IMAGE_SERVER.'/', $match)){
                    $content = str_replace($match, self::BRAND_NAME, $content);
                }
            }
        }
        if(preg_match_all('/[^\s]*\.vn/', $content, $matches)){
            foreach($matches[0] as $match){
                if(!preg_match('/'.self::IMAGE_SERVER.'/', $match)){
                    $content = str_replace($match, self::BRAND_NAME, $content);
                }
            }
        }

        return $content;
    }

    private function isValidSomu($string){
        $string = trim($string);

        if(!$string) return false;

        if(mb_strpos($string, '\)') !== false) return false;

        return true;
    }

    public function download($url, $type, $id){
        try {
            if(mb_strpos($url, 'http://c1.toppick.vn/') !== false) return $url;
            if(mb_strpos($url, 'http://dev.data.giaingay.io/') !== false) return $url;

            $client = new Client();

            $main_server = config('crawl.main_server');

            $response = $client->request('POST', $main_server.'api/v1/multipart', [
                'form_params' => [
                    'url' => $url,
                    'type' => $type,
                    'id' => $id
                ]
            ]);

            $res = json_decode(trim($response->getBody()->getContents()));
            return $res->data;
        } catch (GuzzleException $e) {
            return '#';
        } catch (\Exception $e) {
            return '#';
        }
    }

    public function convertLink($link){
        if(!$link) return null;

        $domain = $this->domain;

        if(!$domain) return null;

        $absolute_url = $this->rel2abs($link, $domain);
        return $absolute_url;
    }


    private function rel2abs($rel, $base)
    {
        /* return if already absolute URL */
        if (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;

        /* queries and anchors */
        if ($rel[0]=='#' || $rel[0]=='?') return $base.$rel;

        /* parse base URL and convert to local variables:
           $scheme, $host, $path */
        $parse_url = parse_url($base);
        $scheme = $parse_url['scheme'];
        $host = $parse_url['host'];
        $path = $parse_url['path'];

        /* remove non-directory element from path */
        $path = preg_replace('#/[^/]*$#', '', $path);

        /* destroy path if relative url points to root */
        if ($rel[0] == '/') $path = '';

        /* dirty absolute URL */
        $abs = "$host$path/$rel";

        /* replace '//' or '/./' or '/foo/../' with '/' */
        $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
        for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

        $abs = str_replace('/../', '/', $abs);

        /* absolute URL is ready! */
        return $scheme.'://'.$abs;
    }

}
