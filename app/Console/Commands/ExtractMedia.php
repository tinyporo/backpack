<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:media
    {--ids=1 : Lựa chọn khoảng id sẽ chạy ids=1 hoặc ids=1-100 , ids=1,2,3,4 }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ids = $this->option('ids');
        $ids = explode('-', $ids );
        $ids = array_filter( $ids );

        if ( count( $ids ) == 1 ){
            $ids = explode( ",", $ids[ 0 ]);

            foreach($ids as $id){
                $this->extractImage( $id);
            }
        }
        elseif ( count( $ids ) == 2 ){

            for ($id = $ids[0]; $id < $ids[1]; $id++){
                $this->extractImage( $id);
            }

        }


    }

    private function extractImage($id){
        $post = \DB::table('all_posts')->where('id', $id)->first();
        if(!$post) return;

        $hoi_dap_id = $post->hoi_dap_id;

        $problem_folder = "/var/www/html/TestProject/storage/Media/Problems/problem_id_$hoi_dap_id";
        $local_problem_folder = "/Applications/MAMP/htdocs/TestProject/storage/Media/Problems/problem_id_$hoi_dap_id";
        $solution_folder = "/var/www/html/TestProject/storage/Media/Solutions/solution_id_$hoi_dap_id";
        $local_solution_folder = "/Applications/MAMP/htdocs/TestProject/storage/Media/Solutions/solution_id_$hoi_dap_id";

        $host = '42.113.207.194';
        $port = 21;
        $user = 'duongtv3';
        $pass = 'topica@dev1p2eco123';

        $ftp = ftp_connect($host,$port);
        if(!$ftp) return 'ftp connect fail!';
        $login_result = ftp_login($ftp,$user,$pass);
        if(!$login_result) return 'login fail!';

        $this->ftp_sync($ftp, $problem_folder, $local_problem_folder);
        $this->ftp_sync($ftp, $solution_folder, $local_solution_folder);
    }

    private function ftp_sync ($ftp, $dir, $local_problem_folder) {

        if ($dir != ".") {
            try{
                ftp_chdir($ftp, $dir);
            }catch (\Exception $e){
                echo ("Change Dir Failed: $dir<BR>\r\n");
                return;
            }

            if (!(is_dir($local_problem_folder))) mkdir($local_problem_folder);
            chdir ($local_problem_folder);
        }

        $contents = ftp_nlist($ftp, ".");
        foreach ($contents as $file) {
            if ($file == '.' || $file == '..')
                continue;

            if (@ftp_chdir($ftp, $file)) {
                ftp_chdir ($ftp, "..");
                $this->ftp_sync ($ftp, $file, $local_problem_folder);
            }
            else
                ftp_get($ftp, $file, $file, FTP_BINARY);
        }

        ftp_chdir ($ftp, "..");
        chdir ("..");
    }
}
