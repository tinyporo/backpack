<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class UploadData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:data
     {--min=1 : chọn id tối thiểu upload data}
     {--mode=insert : chọn mode upload data}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $folder = scandir(storage_path('Media3112'));
//        $folder = array_filter($folder, function($name){return !in_array($name, ['.', '..']);});
//
//        foreach ($folder as $f){
//            $post = \DB::table('all_posts')
//                ->where('hoi_dap_id', $f)->first();
//
//            if(!$post) {
//                dump($f);
//                continue;
//            }
//
//            if($post->id < 5895) dump($f);
//
//        }
//
//        dd('a');
        $mode = $this->option('mode');
        $min = $this->option('min');

        $all_data = \DB::table('data')->where('id', '>=', $min)->get();
        /** @var Collection $all_data */
        $all_data = $all_data->shuffle();

//        $limit = 10000;
        foreach ($all_data as $data){
            $table_name = config("data.table_name.".$data->crawler);

            $post = \DB::table($table_name)->where('hoi_dap_id',$data->hoi_dap_id)->first();
            if(!$post){
//                dump($data);
                continue;
            }

            $url = $post->url;
            $crawler = $post->crawler;


            $title = $post->title;
            $title = str_ireplace(['loigiaihay.com', 'loigiaihay', 'vietjack.com', 'vietjack'], '', $title);
            if($crawler != 'pdf') {
                $title = preg_replace('/\|.*/', '', $title);
                $title = str_ireplace(['-'], '', $title);
            }
            $title = trim($title);
            $subject_new_format = $data->data;
            $content_html = $data->content;

            try{
                if(\DB::table('all_posts')->where('hoi_dap_id', $post->hoi_dap_id)->count() > 0){
                    if($mode !== 'update') continue;

                    \DB::table('all_posts')->where('hoi_dap_id', $post->hoi_dap_id)
                        ->update([
                            'de_bai' => $subject_new_format,
                            'dap_an' => $content_html,
                        ]);
                }else{
                    \DB::table('all_posts')->insert([
                        'tieu_de' => $title,
                        'url' => $url,
                        'de_bai' => $subject_new_format,
                        'ten_nguon' => $crawler,
                        'dap_an' => $content_html,
                        'count' => $post->count,
                        'hoi_dap_id' => $post->hoi_dap_id,
                        'duong_dan_hoi' => 'media/'.$post->hoi_dap_id.'-CH-01.jpg',
                        'duong_dan_tra_loi' => 'media/'.$post->hoi_dap_id.'-DA-01-D.jpg'
                    ]);

                    if($crawler == 'pdf'){
                        $inserted = \DB::table('all_posts')->where('hoi_dap_id', $post->hoi_dap_id)->first();

                        if(!$inserted) break;

                        \DB::table('hoi_dap')
                            ->where('hoi_dap_id', $post->hoi_dap_id)
                            ->update([
                                'toppick_id' => $inserted->id
                            ]);
                    }
                }

//                $limit--;
            }catch (\Exception $e){
                dump($e->getMessage());
            }

//            if($limit === 0) break;
        }

        dd('done');
    }
}
