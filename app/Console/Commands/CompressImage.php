<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CompressImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'compress:image
    {--dir= : chọn thư mục để nén ảnh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir = $this->option('dir');

        if(!is_dir($dir)) return;

        $this->compressImageInFolder($dir);
    }

    private function compressImageInFolder($dir){
        $dir = preg_replace('/\/+$/', '', $dir);

        foreach (glob($dir.'/*') as $file_path){
            if(is_dir($file_path)) $this->compressImageInFolder($file_path);

            $file_path = str_replace(' ', '\ ', $file_path);
            exec("convert -strip -interlace Plane -gaussian-blur 0.05 -quality 85% ".$file_path." ".$file_path);
        }
    }
}
