<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;

class HandleImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'handle:image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $client;

    const DOLLAR_SYMBOL = '---DOLLAR-SYMBOL---';

    public function __construct()
    {
        $this->client = new Client();

        parent::__construct();
    }

    private $id;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hoi_daps = \DB::table('hoi_dap')->where('is_handled', 0)->get();

        foreach ($hoi_daps as $hoi_dap){
            $this->id = $hoi_dap->hoi_dap_id;

            $image_storage_dir = storage_path("media/".$this->id);

            if(is_dir($image_storage_dir)){
                $de_bai = $hoi_dap->de_bai;
                $de_bai = $this->precheckValid($de_bai);
                if(!$de_bai){
                    \DB::table('hoi_dap')->where('id', $hoi_dap->id)
                        ->update([
                            'is_handled' => -1
                        ]);

                    continue;
                }

                $de_bai = $this->handleImage($de_bai, 'Problems');
                $de_bai = str_replace(self::DOLLAR_SYMBOL, '$', $de_bai);

                $dap_an = $hoi_dap->dap_an;
                $dap_an = $this->precheckValid($dap_an);
                if(!$dap_an){
                    \DB::table('hoi_dap')->where('id', $hoi_dap->id)
                        ->update([
                            'is_handled' => -1
                        ]);

                    continue;
                }
                $dap_an = $this->handleImage($dap_an, 'Solutions');
                $dap_an = str_replace(self::DOLLAR_SYMBOL, '$', $dap_an);


                \DB::table('hoi_dap')->where('id', $hoi_dap->id)
                    ->update([
                        'de_bai_full' => $de_bai,
                        'dap_an_full' => $dap_an,
                        'is_handled' => 1
                    ]);

                //upload lên server

                try{
                    $url = '';

                    $book = \DB::table('pdf_book')
                        ->where('book_code', $hoi_dap->book_code)->first();

                    if($book) $url = $book->gd_link;

                    \DB::table('pdf_posts')
                        ->insert([
                            'title' => $hoi_dap->extra_info,
                            'url' => $url,
                            'subject_html' => $de_bai,
                            'content_html' => $dap_an,
                            'crawler' => 'pdf',
                            'data' => '',
                            'is_handled' => 1,
                            'hoi_dap_id' => $hoi_dap->hoi_dap_id
                        ]);
                }catch (\Exception $e){
                    dump($e->getMessage());
                }
            }

        }

    }

    private function precheckValid($content){
        if(preg_match_all('/\$/', $content, $matches)){
            if(count($matches[0]) % 2 === 1){
                return false;
            }

            if(preg_match_all('/\$[^\$]*\$/', $content, $matches)){
                foreach ($matches[0] as $formula){
                    $replace = preg_replace('/^\$/', '\(', $formula);
                    $replace = preg_replace('/\$$/', '\)', $replace);

                    $content = str_replace($formula, $replace, $content);
                }
            }
        }

        return $content;
    }

    private function handleImage($content, $type){
        $content = str_replace("\r", ' ', $content);
        $content = str_replace(' ', ' ', $content);
        $content = str_replace('
', ' ', $content);
//        $content = str_replace("\n", ' ', $content);
        $content = str_replace("\xc2\xa0", ' ', $content);
        $content = str_replace("&#13;", ' ', $content);

        //download ảnh
        if(preg_match_all('/<img[^>]*>/', $content, $matches)){

            foreach ($matches[0] as $img_html){
                $new_html = '';

                if(preg_match('/src="[^"]+"/', $img_html, $matches)){
                    $src_html = $matches[0];

                    if(strpos($src_html, '.css') !== false
                    or strpos($src_html, '.js') !== false ){
                        $new_html = '';
                    }else{
                        if(preg_match('/(?<=src=").*(?=")/', $src_html, $matches)){
                            $src = $matches[0];
                            $src = preg_replace('/^[^\/]*/', $this->id, $src);

                            $src = storage_path("media").'/'.$src;

                            $new_src = $this->downloadImage($src, $type, $this->id);

                            $new_html = '<img src="'.$new_src.'" />';

                        }
                    }

                }

                $content = str_replace($img_html, $new_html, $content);

            }
        }

        return $content;
    }

    private function getLatex($url){
        try{
            $type = pathinfo($url, PATHINFO_EXTENSION);

            $data = file_get_contents($url);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            $response = $this->client->post('42.113.207.170:5000/formula_search', [
                RequestOptions::JSON => ['image' => $base64]
            ]);

            $res = json_decode($response->getBody()->getContents());

            $latex_confidence = $res->latex_confidence;

            if($latex_confidence < 0.7) return '';

            $latex = $res->latex;

            return $latex;
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }

    private function downloadImage($url, $type, $id){

        try {
            $image_server = config('crawl.image_server', 'http://dev.data.giaingay.io/TestProject/public/api/v1/upload_multipart');

            $response = $this->client->request('POST', $image_server, [
                'multipart' => [
                    [
                        'name'     => 'file',
                        'contents' => fopen($url, 'r'),
                    ],
                    [
                        'name'     => 'id',
                        'contents' => $id
                    ],
                    [
                        'name'     => 'type',
                        'contents' => $type
                    ],
                ],
            ]);

            $res = json_decode(trim($response->getBody()->getContents()));
            return $res->data;
        } catch (GuzzleException $e) {
            dump($e->getMessage());
            return '#';
        } catch (\Exception $e) {
            dump($e->getMessage());

            return '#';
        }
    }
}
