<?php

namespace App\Console;

use App\Console\Commands\CompressImage;
use App\Console\Commands\DeleteImage;
use App\Console\Commands\ExtractMedia;
use App\Console\Commands\GetData;
use App\Console\Commands\HandleImage;
use App\Console\Commands\UpdateData;
use App\Console\Commands\UploadData;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        HandleImage::class,
        ExtractMedia::class,
        GetData::class,
        UpdateData::class,
        UploadData::class,
        DeleteImage::class,
        CompressImage::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
