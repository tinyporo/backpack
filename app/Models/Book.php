<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Book extends Model
{
    use CrudTrait;

    const IS_HARD_COPY = 1;
    const NOT_HARD_COPY = 0;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'books';
    // protected $primaryKey = 'id';
     public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'isbn',
        'class',
        'subject',
        'category',
        'tap',
        'chuyen',
        'province',
        'publisher',
        'pub_year',
        'republish',
        'author',
        'language',
        'country',
        'level',
        'status',
        'description',
        'is_hard_copy',
        'digital_copy_url',
        'inserted_by',
        'inserted_date',
        'modified_by',
        'modified_date',
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function classes(){
        return $this->belongsTo('App\Models\Classes', 'class');
    }

    public function subject(){
        return $this->belongsTo('App\Models\Subject', 'subject');
    }

    public function book_category(){
        return $this->belongsTo('App\Models\BookCategory', 'category');
    }

    public function my_province(){
        return $this->belongsTo('App\Models\Province', 'province');
    }

    public function my_publisher(){
        return $this->belongsTo('App\Models\Publisher', 'publisher');
    }

    public function my_author(){
        return $this->belongsTo('App\Models\Author', 'author');
    }

    public function my_language(){
        return $this->belongsTo('App\Models\Country', 'language', 'lang_code');
    }

    public function my_country(){
        return $this->belongsTo('App\Models\Country', 'country', 'code');
    }

    public function my_level(){
        return $this->belongsTo('App\Models\Level', 'level');
    }



    public function activity_logs(){
        return $this->hasMany('App\Models\BookStoreActivityLog', 'book');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
