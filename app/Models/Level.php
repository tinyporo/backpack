<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\Level
 *
 * @property Level $children
 * @property Level $parent
 * @method static Builder where()
 */
class Level extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'levels';
    // protected $primaryKey = 'id';
     public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name', 'description', 'parent_id'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function isRootLevel(){
        $prev_level = $this->getPrevLevel();

        if(!$prev_level) return true;
        return false;
    }

    public function getPrevLevel(){
        $prev_level = $this->parent;

        return $prev_level;
    }

    public function getNextLevel(){
        $next_levels = $this->children;

        foreach ($this->getSiblingLevels() as $siblingLevel){
            $next_levels = $next_levels->merge($siblingLevel->children);
        }

        return $next_levels;
    }

    public function getSiblingLevels(){
        if($this->isRootLevel()) $siblings = Level::whereNull('parent_id')->get();
        else{
            $parent_level = $this->getPrevLevel();

            $siblings = $parent_level->children;

            $siblings = $siblings->filter(function($level){
                return $level->name != $this->name;
            });
        }

        return $siblings;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function book(){
        return $this->hasMany('App\Models\Book', 'level');
    }

    public function all_posts(){
        return $this->hasMany('App\Models\AllPost', 'level', 'name');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Level', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Level', 'parent_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
