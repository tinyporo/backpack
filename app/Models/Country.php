<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Country extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'countries';
    // protected $primaryKey = 'id';
     public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name', 'code', 'lang_code', 'lang_name'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function book(){
        return $this->hasMany('App\Models\Book', 'language', 'lang_code')
            || $this->hasMany('App\Models\Book', 'country', 'code');
    }

    public function classes(){
        return $this->hasMany('App\Models\Classes', 'country', 'code');
    }

    public function subject(){
        return $this->hasMany('App\Models\Subject', 'country', 'code');
    }

    public function book_category(){
        return $this->hasMany('App\Models\BookCategory', 'country', 'code');
    }

    public function my_province(){
        return $this->hasMany('App\Models\Province', 'country', 'code');
    }

    public function publisher(){
        return $this->hasMany('App\Models\Publisher', 'country', 'code');
    }

    public function author(){
        return $this->hasMany('App\Models\Author', 'bookcodeprint_country', 'code');
    }

    public function bookcodeprint_country(){
        return $this->hasMany('App\Models\BookCodePrint', 'country', 'code');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
