<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class HoiDap extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'hoi_dap';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'hoi_dap_id',
        'book_code',
        'toppick_id',
        'extra_info',
        'de_bai',
        'dap_an',
        'duong_dan_hoi',
        'duong_dan_tra_loi',
        'created_by'
    ];

    public static $item_prefix_code = 'IT';
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    function getTpId(){
        if($this->toppick_id) return $this->toppick_id;

        $post = \DB::table('all_posts')->where('hoi_dap_id', $this->hoi_dap_id)->first();
        if($post) {
            \DB::table('hoi_dap')->where('id', $this->id)
                ->update([
                    'toppick_id' => $post->id
                ]);

            return $post->id;
        }

        return 'N/A';
    }

    public static function getNextGuid(){
        $next_id = \DB::table('INFORMATION_SCHEMA.TABLES')
            ->select('AUTO_INCREMENT')
            ->where('TABLE_SCHEMA', 'backpack')
            ->where('TABLE_NAME', 'guid')
            ->first()->AUTO_INCREMENT;

        return $next_id;
    }

    private static $guild = null;


    public static function GUID()
    {
        if(is_null(self::$guild)) {
            $next_guid = str_random(9).uniqid('', true);

            self::$guild = str_pad($next_guid,32,"0",STR_PAD_LEFT);
        }

        return self::$guild;
    }

    public static function getNextQuestionGuid(){
        return self::$item_prefix_code.self::GUID()."-CH-01";
    }

    public static function getNextQuestionPath(){
        return "media/".self::getNextQuestionGuid().".jpg";
    }

    public static function getNextAnswerGuid(){
        return self::$item_prefix_code.self::GUID()."-DA-01-D";
    }

    public static function getNextAnswerPath(){
        return "media/".self::getNextAnswerGuid().".jpg";
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function pdf_book(){
        return $this->belongsTo('App\Models\PdfBook', 'book_code', 'book_code');
    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

//    public function getToppickIdAttribute(){
//        if($this->toppick_id) return $this->toppick_id;
//
//        $client = new Client();
//
//        $res = $client->request('POST', 'http://c1.toppick.vn/topica_crawler/public/api/get_tp_id', [
//            'form_params' => [
//                'hoi_dap_id' => $this->hoi_dap_id
//            ]
//        ]);
//
//        try{
//            $toppick_id = json_decode($res->getBody()->getContents())->data;
//
//            \DB::table('hoi_dap')->where('id', $this->id)
//                ->update('toppick_id', $toppick_id);
//
//            return $toppick_id;
//        }catch (\Exception $e){
//            return 'N/A';
//        }
//    }
}
