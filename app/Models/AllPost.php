<?php

namespace App\Models;

use App\Helpers\DateIntervalHelper;
use App\Helpers\HasTags;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Spatie\Tags\Tag;

/**
 * App\Models\AllPost
 *
 * @property integer $id
 * @property string $hoi_dap_id
 * @property string $level
 * @property Carbon $created_at
 * @property Level $my_level
 * @method static AllPost | boolean find($id)
 */

class AllPost extends Model
{
    use CrudTrait;
    use HasTags;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'all_posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
        'hoi_dap_id',
        'tieu_de',
        'url',
        'de_bai',
        'ten_nguon',
        'dap_an',
        'duong_dan_hoi',
        'duong_dan_tra_loi',
        'count',
        'type',
        'level'
    ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getGiaingayLink(){
        return '<a target="_blank" href="http://dev.data.giaingay.io/crawlerview/index.php?hoi_dap_id='.$this->hoi_dap_id.'">'.$this->tieu_de.'</a>';
    }

    public function upLevel()
    {
        $html = '<select id="'.$this->id.'" class="custom-select custom-select-lg mb-3">';

        $levels = Level::get();

        foreach ($levels as $level){
            if($level->name === $this->level) $html .= '<option value="'.$level->name.'" selected>'.$level->name.'</option>';
            else $html .= '<option value="'.$level->name.'">'.$level->name.'</option>';
        }

        $html .= "</select>
                <script>
                    $('select#".$this->id."').on('change', function() {
                      let value = this.value;
                    
                      window.location.href = \"".route('allpost_uplevel', ['id'=>$this->id, 'level'=>''])."\" + '/' + value".";
                    });
                </script>";

        return $html;
    }

    public function getAverageUpTime(){
        return '<a href="'.route('show_histories', ['id' => $this->id]).'" class="btn btn-primary ladda-button" style="min-width: 20rem; min-height: 3.2rem" data-style="zoom-in"><span class="ladda-label">'.$this->calAverageUpTime().'</span></a>';
    }

    public function calAverageUpTime(){
        $histories_builder = \DB::table('level_up_histories')->where('hoi_dap_id', $this->hoi_dap_id)
            ->orderBy('created_at', 'desc');

        $last_history = $histories_builder->first();

        $average_time = 0;

//        foreach ($histories as $history){
//            $level_name = $history->level;
//            $level = Level::where('name', $level_name)->first();
//
//            if(!$level) continue;
//
//            /** @var Level $level */
//            if($level->parent->isRootLevel()){
//                $start = $this->created_at;
//
//            }else{
//                $prev_level = $level->getPrevLevel();
//                if(!$prev_level) continue;
//
//                $prev_history = \DB::table('level_up_histories')
//                    ->where('hoi_dap_id', $this->hoi_dap_id)
//                    ->where('level', $prev_level->name)
//                    ->first();
//
//                $start = $prev_history->created_at;
//            }
//
//            $end = $history->created_at;
//
//            $differ_time = $this->getDifferTime($start, $end);
//
//            $total_time += $differ_time;
//        }

        if($last_history){
            $start = $this->created_at;
            $end = $last_history->created_at;

            $total_time = $this->getDifferTime($start, $end);

            $average_time = $total_time / $histories_builder->count();
        }


//        if(count($histories) != 0) $average_time = $total_time/count($histories);
//        else $average_time = $total_time;

        return DateIntervalHelper::format($average_time);
    }

    private function getDifferTime($start, $end){
        $timestamp1 = strtotime($start);
        $timestamp2 = strtotime($end);

        return $timestamp2 - $timestamp1;
    }

    public function hasMaxLevel(){
        $next_levels = $this->getNextLevel();

        if($next_levels) return false;
        return true;
    }

    public function getNextLevel(){
        $level = $this->my_level;

        if(!$level) return false;

        $next_levels = $level->getNextLevel();

        if($next_levels->count() > 0) return $next_levels[0];
        return false;

    }

    public function levelUp($level = null){
        if($this->hasMaxLevel()) return false;

        try{
            if(!$level) $next_level = $this->getNextLevel();
            else $next_level = Level::where('name', $level)->first();

            if(!$next_level || $next_level->count() === 0) return false;

            $this->level = $next_level->name;

            $this->save();

            \DB::table('level_up_histories')
                ->insert([
                    'hoi_dap_id' => $this->hoi_dap_id,
                    'level' => $next_level->name,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            return true;
        }catch (\Exception $e){
            return false;
        }

    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function my_level(){
        return $this->belongsTo('App\Models\Level', 'level', 'name');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    public function getTagsAttribute(){

        $tags = $this->taggs->map(function (Tag $item) {
                return $item->name;
            })->toArray();

        $tags = implode(',', $tags);

        return $tags;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
