<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class BookCodePrint extends Model
{
    use CrudTrait;
    const BAR_CODE = 'Mã vạch';
    const QR_CODE = 'Mã QR code';
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'bookcodeprints';
    // protected $primaryKey = 'id';
    public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['bookcodeprint_book_id','bookcodeprint_inserted_by','bookcodeprint_modified_by','bookcodeprint_country','bookcodeprint_type','bookcodeprint_status','bookcodeprint_inserted_date','bookcodeprint_modified_date','bookcodeprints_type_code'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getLastId()
    {
        $db = \DB::getDatabaseName();
        $next_id = \DB::table('INFORMATION_SCHEMA.TABLES')
            ->select('AUTO_INCREMENT')
            ->where('TABLE_SCHEMA', $db)
            ->where('TABLE_NAME', $this->table)
            ->first()->AUTO_INCREMENT;
        return $next_id;
    }
    /**
     * Get code country (mã quốc gia)
     */
    public function getCodeCountry($country_id)
    {
        return \App\Models\Country::find($country_id)->code;
    }
    /**
     * Get code type (Kiểu tài liệu)
     */
    public function getCodeType($type_file_id)
    {
        return \App\Models\BookCodePrintTypes::find($type_file_id)->bookcodeprinttypes_code;
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function country(){
        return $this->belongsTo('App\Models\Country', 'bookcodeprint_country', 'code');
    }

    public function bookcodeprinttype(){
        return $this->belongsTo('App\Models\BookCodePrintTypes', 'bookcodeprint_type', 'bookcodeprinttypes_code');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
