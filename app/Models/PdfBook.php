<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class PdfBook extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'pdf_book';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['book_code', 'book_name', 'gd_link', 'type', 'created_by'];
    // protected $hidden = [];
    // protected $dates = [];

    public static $book_prefix_code = 'BK';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public static function getLastId()
    {
        $db = \DB::getDatabaseName();

        $next_id = \DB::table('INFORMATION_SCHEMA.TABLES')
            ->select('AUTO_INCREMENT')
            ->where('TABLE_SCHEMA', $db)
            ->where('TABLE_NAME', 'pdf_book')
            ->first()->AUTO_INCREMENT;
        return $next_id;
    }

    public function getBookName(){
        return '<a href="'.$this->gd_link.'">'.$this->book_name.'</a>';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function bookcodeprinttype(){
        return $this->belongsTo('App\Models\BookCodePrintTypes', 'type', 'bookcodeprinttypes_code');
    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}
