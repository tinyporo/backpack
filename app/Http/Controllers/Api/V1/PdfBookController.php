<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\PdfBook;
use Illuminate\Http\Request;

class PdfBookController extends Controller
{
    public function create(Request $request){
        $book_type = $request->get('book_type');

        $book_type_obj = \DB::table('bookcodeprinttypes')->where('bookcodeprinttypes_code', $book_type)->first();
        if(!$book_type_obj){
            return [
                'success' => false,
                'book_id' => '',
                'msg' => "Invalid book type code!"
            ];
        }

        $book_name = $request->get('book_name');
        $gd_link = $request->get('gd_link');

        $book_code = 'VN'.$book_type.str_pad(PdfBook::getLastId(),12,'0',STR_PAD_LEFT);

        try{
            \DB::table('pdf_book')
                ->insert([
                    'book_code' => $book_code,
                    'book_name' => $book_name,
                    'type' => $book_type,
                    'gd_link' => $gd_link,
                    'created_by' => 1,
                ]);

            return [
                'success' => true,
                'book_id' => $book_code,
                'msg' => ''
            ];
        }catch (\Exception $e){
            return [
                'success' => false,
                'book_id' => '',
                'msg' => $e->getMessage()
            ];
        }
    }

    public function exist(Request $request){
        $book_id = $request->get('book_id');

        $book = \DB::table('pdf_book')->where('book_code', $book_id)->first();

        if($book) return [
            'success' => true,
            'exists' => true,
        ];

        return [
            'success' => true,
            'exists' => false,
        ];
    }
}
