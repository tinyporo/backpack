<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HoidapController extends Controller
{
    public function create(Request $request)
    {
        $book_code = $request->get('bookid');

        $book_obj = \DB::table('pdf_book')->where('book_code', $book_code)->first();
        if (!$book_obj) {
            return [
                'success' => false,
                'book_id' => '',
                'msg' => "Invalid book id!"
            ];
        }

        $de_bai = $request->get('cau_hoi');
        $dap_an = $request->get('dap_an');
        $extra_info = $request->get('extra_info');

        $file_path = $request->get('file_path');

        $package = $request->get('package');
        $package_info = explode('_', $package, 3);
        if (count($package_info) < 3) {
            return [
                'success' => false,
                'book_id' => '',
                'msg' => "Invalid package!"
            ];
        }

        $nguoi_nhap_lieu = array_get($package_info, 0, '');
        $packageid = array_get($package_info, 1, '');
        $nguon_du_lieu = array_get($package_info, 2, '');

        $hoi_dap_id = str_pad(str_random(9) . uniqid('', true), 32, "0", STR_PAD_LEFT);

        $duong_dan_hoi = "media/$hoi_dap_id-CH-01.jpg";
        $duong_dan_tra_loi = "media/$hoi_dap_id-DA-01-D.jpg";

        try {
            \DB::table('hoi_dap')
                ->insert([
                    'book_code' => $book_code,
                    'de_bai' => $de_bai,
                    'dap_an' => $dap_an,
                    'hoi_dap_id' => $hoi_dap_id,
                    'duong_dan_hoi' => $duong_dan_hoi,
                    'duong_dan_tra_loi' => $duong_dan_tra_loi,
                    'extra_info' => $extra_info,
                    'file_path' => $file_path,
                    'nguoi_nhap_lieu' => $nguoi_nhap_lieu,
                    'packageid' => $packageid,
                    'nguon_du_lieu' => $nguon_du_lieu,
                    'created_by' => 1,
                ]);

            return [
                'success' => true,
                'item_id' => $hoi_dap_id,
                'msg' => ''
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'item_id' => '',
                'msg' => $e->getMessage()
            ];
        }
    }

    public function update(Request $request)
    {
        $book_code = $request->get('bookid');

        $book_obj = \DB::table('pdf_book')->where('book_code', $book_code)->first();
        if (!$book_obj) {
            return [
                'success' => false,
                'book_id' => '',
                'msg' => "Invalid book id!"
            ];
        }

        $de_bai = $request->get('cau_hoi');
        $dap_an = $request->get('dap_an');
        $extra_info = $request->get('extra_info');

        $file_path = $request->get('file_path');

        $package = $request->get('package');
        $package_info = explode('_', $package, 3);
        if (count($package_info) < 3) {
            return [
                'success' => false,
                'book_id' => '',
                'msg' => "Invalid package!"
            ];
        }

        $nguoi_nhap_lieu = array_get($package_info, 0, '');
        $packageid = array_get($package_info, 1, '');
        $nguon_du_lieu = array_get($package_info, 2, '');

        $hoi_dap_id = $request->get('hoi_dap_id');

        $duong_dan_hoi = "media/$hoi_dap_id-CH-01.jpg";
        $duong_dan_tra_loi = "media/$hoi_dap_id-DA-01-D.jpg";

        try {
            \DB::table('hoi_dap')
                ->where('hoi_dap_id', $hoi_dap_id)
                ->insert([
                    'book_code' => $book_code,
                    'de_bai' => $de_bai,
                    'dap_an' => $dap_an,
                    'duong_dan_hoi' => $duong_dan_hoi,
                    'duong_dan_tra_loi' => $duong_dan_tra_loi,
                    'extra_info' => $extra_info,
                    'file_path' => $file_path,
                    'nguoi_nhap_lieu' => $nguoi_nhap_lieu,
                    'packageid' => $packageid,
                    'nguon_du_lieu' => $nguon_du_lieu,
                    'created_by' => 1,
                ]);

            return [
                'success' => true,
                'item_id' => $hoi_dap_id,
                'msg' => ''
            ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'item_id' => '',
                'msg' => $e->getMessage()
            ];
        }
    }

    public function exist(Request $request)
    {
        $item_id = $request->get('item_id');

        $item = \DB::table('hoi_dap')->where('hoi_dap_id', $item_id)->first();

        if ($item) return [
            'success' => true,
            'exists' => true,
        ];

        return [
            'success' => true,
            'exists' => false,
        ];
    }

}
