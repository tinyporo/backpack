<?php
/**
 * Created by PhpStorm.
 * User: conghoan
 * Date: 5/29/18
 * Time: 16:37
 */

namespace App\Http\Controllers\Frontend;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController
{

    public function detail(Request $request, $id)
    {
        $article = Article::find($id);

        return view('detail', ['article' => $article]);
    }
}