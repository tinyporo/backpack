<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookCodePrintRequest as StoreRequest;
use App\Http\Requests\BookCodePrintRequest as UpdateRequest;
use Carbon\Carbon;
use App\Models\BookCodePrint;
use App\Http\Controllers\Admin\ExportXlsController;
use Excel;
/**
 * Class BookCodePrintCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BookCodePrintCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\BookCodePrint');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/book-code-print');   
        $this->crud->setEntityNameStrings('bookcodeprint', 'Tạo mã vạch');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */
        // ------ CRUD COLUMNS
        // $this->crud->addColumn([
        //     'name' => 'id',
        //     'label' => 'ID',
        // ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_book_id',
            'label' => 'Mã code',
        ]);
        
        $this->crud->addColumn([
            'name' => 'bookcodeprint_country',
            'label' => 'Quốc gia',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_type',
            'label' => 'Kiểu tài liệu',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprints_type_code',
            'label' => 'Kiểu mã code',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_status',
            'label' => 'Trạng thái',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_inserted_by',
            'label' => 'Người tạo',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_modified_by',
            'label' => 'Người sửa cuối cùng',
        ]);
        
        $this->crud->addColumn([
            'name' => 'bookcodeprint_modified_date',
            'label' => 'Ngày chỉnh sửa',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprint_inserted_date',
            'label' => 'Ngày tạo',
        ]);
        // FIELDS
        $this->crud->addField([    // SELECT
            'label' => 'Chọn quốc gia',
            'type' => 'select2',
            'name' => 'bookcodeprint_country',
            'entity' => 'country',
            'attribute' => 'name',
            'model' => "App\Models\Country",
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Chọn kiểu tài liệu',
            'type' => 'select2',
            'name' => 'bookcodeprint_type',
            'entity' => 'bookcodeprinttype',
            'attribute' => 'bookcodeprinttypes_code',
            'model' => "App\Models\BookCodePrintTypes",
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Chọn kiểu mã vạch',
            'type' => 'select2',
            'name' => 'bookcodeprints_type_code',
            'type' => 'select_from_array',
            'options' => [BookCodePrint::BAR_CODE => 'Mã vạch', BookCodePrint::QR_CODE  => 'QR code'],
        ]);

        $this->crud->addField([
            'name' => 'bookcodeprint_modified_date',
            'type' => 'hidden',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);
        $this->crud->addField([
            'name' => 'bookcodeprint_inserted_date',
            'type' => 'hidden',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'bookcodeprint_inserted_by',
            'type' => 'hidden',
            'label' => '',
            'default' => \Auth::user()->name,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'bookcodeprint_modified_by',
            'type' => 'hidden',
            'label' => '',
            'default' => \Auth::user()->name,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'bookcodeprint_status',
            'type' => 'hidden',
            'label' => '',
            'default' => 0,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'loop_code',
            'type' => 'number',
            'label' => 'Số mã cần tạo',
            'default' => 1,
            'attributes' => [
                'min' => '1',
            ],
        ]);
        // $this->crud->removeButton( 'preview' );
        $this->crud->removeButton( 'update' );
        // $this->crud->removeButton( 'revisions' );
        $this->crud->removeButton( 'delete' );
        // $this->crud->removeColumn('action');
        // $this->crud->enableAjaxTable();
        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        // $this->crud->removeButton('edit');
        // add asterisk for fields that are required in BookCodePrintRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        // $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        // $this->crud->removeButton('edit');
    }

    /**
     * format number 12.
     */
    private function formatNumber($number)
    {
        return str_pad($number,12,'0',STR_PAD_LEFT);
    }

    public function store(StoreRequest $request)
    {
        $code = new BookCodePrint();
        $numberCode = $code->getLastId();
        
        $loop = $request->request->get('loop_code');
        $country = $request->request->get('bookcodeprint_country');
        $type = $request->request->get('bookcodeprint_type');
        $country = $code->getCodeCountry($country);
        $type = $code->getCodeType($type);
        // dd($type->bookcodeprinttypes_code);
        $listCodes = [];
        # Check if your variable is an integer
        if ( filter_var($loop, FILTER_VALIDATE_INT) === false ) {
            // dd( "Không phải là số");
        }else{
            for ( $i=0; $i< $loop; $i++) {
                $numberCodeCurent = $numberCode;
                $numberCodeCurent +=$i;
                $numberCodeCurent = $this->formatNumber($numberCodeCurent);
                $code = $country.$type.$numberCodeCurent;
                $listCodes[] = ['ma'=>$code,'soluong'=>1];
                $request->request->set('bookcodeprint_book_id',$code);
                $request->request->set('bookcodeprint_country',$country);
                $request->request->set('bookcodeprint_type',$type);
                $data = $request->except(['save_action', '_token', '_method', 'current_tab', 'http_referrer']);
                // dd($data);
                BookCodePrint::create($data);
                // parent::storeCrud($request);
            }
        }
        // dd($request);
        // your additional operations before save here
        // $redirect_location = parent::storeCrud($request);
        return $this->export($listCodes);
        return $redirect_location;

        //////////////////////////////////

        // your additional operations before save here
        //$redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        //return $redirect_location;
    }

    public function export($data) 
    {
        return Excel::download(new ExportXlsController($data), 'in_ma.xls');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        // $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
