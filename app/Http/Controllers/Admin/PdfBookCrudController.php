<?php

namespace App\Http\Controllers\Admin;

use App\Models\PdfBook;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PdfBookRequest as StoreRequest;
use App\Http\Requests\PdfBookRequest as UpdateRequest;

/**
 * Class PdfBookCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PdfBookCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\PdfBook');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/pdf_book');
        $this->crud->setEntityNameStrings('pdfbook', 'pdf_books');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'book_name',
            'label' => 'Tên sách',
            'type'          => "model_function_custom",
            'function_name' => 'getBookName',
        ]);
        $this->crud->addColumn([
            'name' => 'book_code',
            'label' => 'Mã sách',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'label' => 'Loại sách',
            'type'  => "select",
            'entity' => 'bookcodeprinttype',
            'attribute' => 'bookcodeprinttypes_name',
            'model' => "App\Models\BookCodePrintTypes",
        ]);
        $this->crud->addColumn([
            'name' => 'created_by',
            'label' => 'Người tạo',
            'type'  => "select",
            'entity' => 'user',
            'attribute' => 'email',
            'model' => "App\User",
        ]);

        // FIELDS
        $this->crud->addField([    // SELECT
            'label' => 'Chọn kiểu tài liệu',
            'type' => 'select2',
            'name' => 'type',
            'entity' => 'bookcodeprinttype',
            'attribute' => 'bookcodeprinttypes_name',
            'model' => "App\Models\BookCodePrintTypes",
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Tên tài liệu',
            'type' => 'text',
            'name' => 'book_name',
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Link google drive',
            'type' => 'text',
            'name' => 'gd_link',
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'hidden',
            'label' => '',
            'default' => \Auth::user()->id,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        $type = $request->type;
        $book_type = \DB::table('bookcodeprinttypes')->where('id', $type)->first();
        if(!$book_type){
            return redirect()->back();
        }

        $book_code = $book_type->bookcodeprinttypes_code;

        $request->request->set('type', $book_code);
        $request->request->add(['book_code' => PdfBook::$book_prefix_code.$book_code.str_pad(PdfBook::getLastId(),12,'0',STR_PAD_LEFT)]);

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $type = $request->type;
        $book_type = \DB::table('bookcodeprinttypes')->where('id', $type)->first();
        if(!$book_type){
            return redirect()->back();
        }

        $book_code = $book_type->bookcodeprinttypes_code;

        $request->request->add(['type' => $book_code]);

        PdfBook::where('id', $request->id)->update($request->except('save_action', '_token', '_method', 'current_tab', 'http_referrer'));

        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        $redirect_location = $this->performSaveAction($request->id);

        return $redirect_location;
    }
}
