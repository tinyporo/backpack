<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookCodePrintTypesRequest as StoreRequest;
use App\Http\Requests\BookCodePrintTypesRequest as UpdateRequest;
use Carbon\Carbon;

/**
 * Class BookCodePrintTypesCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BookCodePrintTypesCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\BookCodePrintTypes');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/file-type');
        $this->crud->setEntityNameStrings('bookcodeprinttypes', 'Kiểu tài liệu');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        
        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'bookcodeprinttypes_name',
            'label' => 'Tên kiểu tài liệu',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprinttypes_code',
            'label' => 'Mã code',
        ]);
        $this->crud->addColumn([
            'name' => 'bookcodeprinttypes_inserted_by',
            'label' => 'Người tạo',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'label' => 'Ngày chỉnh sửa',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Ngày tạo',
        ]);

        // FIELDS
        $this->crud->addField([    // SELECT
            'label' => 'Tên kiểu tài liệu',
            'type' => 'text',
            'name' => 'bookcodeprinttypes_name',
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Mã code',
            'type' => 'text',
            'name' => 'bookcodeprinttypes_code',
        ]);

        $this->crud->addField([
            'name' => 'bookcodeprinttypes_inserted_by',
            'type' => 'hidden',
            'label' => '',
            'default' => \Auth::user()->name,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'created_at',
            'type' => 'hidden',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        $this->crud->addField([
            'name' => 'updated_at',
            'type' => 'hidden',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);



        // add asterisk for fields that are required in BookCodePrintTypesRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
