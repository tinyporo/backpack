<?php

namespace App\Http\Controllers\Admin;

use App\Models\HoiDap;
use App\Models\PdfBook;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\HoiDapRequest as StoreRequest;
use App\Http\Requests\HoiDapRequest as UpdateRequest;

/**
 * Class HoiDapCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class HoiDapCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\HoiDap');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/hoi_dap');
        $this->crud->setEntityNameStrings('câu hỏi đáp', 'các câu hỏi đáp');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'hoi_dap_id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'toppick_id',
            'label' => 'Id trên toppick',
            'type'          => "model_function_custom",
            'function_name' => 'getTpId',
        ]);
        $this->crud->addColumn([
            'name' => 'extra_info',
            'label' => 'Thông tin thêm',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'label' => 'Tên sách sách',
            'type'  => "select",
            'entity' => 'pdf_book',
            'attribute' => 'book_code',
            'model' => "App\Models\PdfBook",
        ]);
        $this->crud->addColumn([
            'name' => 'de_bai',
            'label' => 'Đề bài',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'dap_an',
            'label' => 'Đáp án',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'duong_dan_hoi',
            'label' => 'Đường dẫn câu hỏi',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'duong_dan_tra_loi',
            'label' => 'Đường dẫn câu trả lời',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'created_by',
            'label' => 'Người tạo',
            'type'  => "select",
            'entity' => 'user',
            'attribute' => 'email',
            'model' => "App\User",
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'hoi_dap_id',
            'label' => '',
            'default' => HoiDap::GUID(),
            'type'          => "text",
            'attributes' => [
                'style' => 'display: none',
            ]
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'GU_ID',
            'label' => 'ID của tài liệu này: ',
            'default' => HoiDap::GUID(),
            'type'          => "copy_clipboard",
            'attributes' => [
                'style' => 'display: inline-block; width: 35%;',
            ]
        ]);
        $this->crud->addField([
            'name' => 'book_code',
            'label' => 'Sách: ',
            'type'  => "select",
            'entity' => 'pdf_book',
            'attribute' => 'book_code',
            'model' => "App\Models\PdfBook",
            'default' => session()->has('last_book_code') ? session()->get('last_book_code') : null
        ]);
        $this->crud->addField([
            'name' => 'extra_info',
            'label' => 'Thông tin thêm:',
            'type' => 'text',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'GUID_de_bai',
            'label' => 'ID của đề bài này: ',
            'default' => HoiDap::getNextQuestionGuid(),
            'type'          => "copy_clipboard",
            'attributes' => [
                'style' => 'display: inline-block; width: 45%;',
            ]
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'de_bai',
            'label' => 'Đề bài: ',
            'type'           => 'ckeditor',
            'extra_plugins'  => ['oembed', 'widget', 'ckeditor_wiris'],
            'placeholder'    => 'Nhập câu hỏi',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'GUID_dap_an',
            'label' => 'ID của đáp án này: ',
            'default' => HoiDap::getNextAnswerGuid(),
            'type'          => "copy_clipboard",
            'attributes' => [
                'style' => 'display: inline-block; width: 45%;',
            ]
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'dap_an',
            'label' => 'Đáp án: ',
            'type'           => 'ckeditor',
            'extra_plugins'  => ['oembed', 'widget', 'ckeditor_wiris'],
            'placeholder'    => 'Nhập câu hỏi',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'duong_dan_hoi',
            'label' => 'Đường dẫn câu hỏi: ',
            'default' => HoiDap::getNextQuestionPath(),
            'type'          => "copy_clipboard",
            'attributes' => [
                'style' => 'display: inline-block; width: 45%;',
            ]
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'duong_dan_tra_loi',
            'label' => 'Đường dẫn câu trả lời: ',
            'default' => HoiDap::getNextAnswerPath(),
            'type'          => "copy_clipboard",
            'attributes' => [
                'style' => 'display: inline-block; width: 45%;',
            ]
        ]);

        $this->crud->addField([
            'name' => 'created_by',
            'type' => 'hidden',
            'label' => '',
            'default' => \Auth::user()->id,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);

        //Filder
        $this->crud->addFilter([
            'name' => 'hoi_dap_id',
            'type' => 'text',
            'label'=>'Tìm kiếm theo ID'
        ],false,function($value){
            $this->crud->addClause('where','hoi_dap_id','LIKE', "%$value%");
        });

        $this->crud->addFilter([
            'name' => 'toppick_id',
            'type' => 'text',
            'label'=>'Tìm kiếm theo ID Toppick'
        ],false,function($value){
            $this->crud->addClause('where','toppick_id','LIKE', "%$value%");
        });

        $this->crud->addFilter([
            'name' => 'package_id',
            'type' => 'text',
            'label'=>'Tìm kiếm theo package id'
        ],false,function($value){
            $package_info = explode('_', $value, 3);
            if(count($package_info) < 3){
                return ;
            }else{
                $nguoi_nhap_lieu = array_get($package_info, 0 , '');
                $packageid = array_get($package_info, 1 , '');
                $nguon_du_lieu = array_get($package_info, 2 , '');

                $this->crud->addClause('where','nguoi_nhap_lieu','LIKE', "%$nguoi_nhap_lieu%");
                $this->crud->addClause('where','packageid','LIKE', "%$packageid%");
                $this->crud->addClause('where','nguon_du_lieu','LIKE', "%$nguon_du_lieu%");
            }
        });

        $this->crud->enableAjaxTable();

        $this->crud->removeButton('delete');
//        $this->crud->removeButton('update');
    }

    public function store(StoreRequest $request)
    {
        $book_code = $request->book_code;

        session()->put('last_book_code', $book_code);

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);

        \DB::table('hoi_dap')
            ->where('id', $this->crud->entry->id)
            ->update(['book_code' => PdfBook::find($request->book_code)->book_code]);

        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
