<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LevelRequest as StoreRequest;
use App\Http\Requests\LevelRequest as UpdateRequest;

/**
 * Class LevelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class LevelCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Level');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/level');
        $this->crud->setEntityNameStrings('level', 'levels');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('name', 2);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Tên level',
            'type'  => "text",
        ]);

        $this->crud->addColumn([
            'name' => 'description',
            'label' => 'Mô tả',
            'type'  => "text",
            'limit' => 50
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Tên level: ',
            'type'  => "text",
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => 'Mô tả: ',
            'type'  => "textarea",
        ]);

        $this->crud->addField([
            'name' => 'parent_id',
            'label' => 'Level trước: ',
            'type'  => "select",
            'entity' => 'parent',
            'attribute' => 'name',
            'model' => "App\Models\Level",
        ]);

        $this->crud->orderBy('name','asc');

        // add asterisk for fields that are required in LevelRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
