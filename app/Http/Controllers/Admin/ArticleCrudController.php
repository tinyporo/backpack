<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ArticleRequest as StoreRequest;
use App\Http\Requests\ArticleRequest as UpdateRequest;

class ArticleCrudController extends CrudController
{
    public function __construct()
    {
        parent::__construct();

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel("App\Models\Article");
        $this->crud->setRoute(config('backpack.base.route_prefix', 'admin').'/article');
        $this->crud->setEntityNameStrings('câu hỏi', 'câu hỏi');

        /*
        |--------------------------------------------------------------------------
        | COLUMNS AND FIELDS
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
                                'name' => 'title',
                                'label' => 'Câu hỏi',
                                'type'          => "model_function_custom",
                                'function_name' => 'getTitleText',
                            ]);
        $this->crud->addColumn([
            'name' => 'content',
            'label' => 'Câu trả lời',
            'type'          => "model_function_custom",
            'function_name' => 'getContentText',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'label' => 'Ngày tạo',
            'type' => 'date',
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([
            'name'           => 'title',
            'label'          => 'Câu hỏi',
            'type'           => 'ckeditor',
            'extra_plugins'  => ['oembed', 'widget', 'ckeditor_wiris'],
            'placeholder'    => 'Nhập câu hỏi',
        ]);

        $this->crud->addField([    // WYSIWYG
                                'name' => 'content',
                                'label' => 'Câu trả lời',
                                'type' => 'ckeditor',
                                'extra_plugins'  => ['oembed', 'widget', 'ckeditor_wiris'],
                                'placeholder' => 'Nhập câu trả lời ở đây',
                            ]);

        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
//        return parent::storeCrud($request);

        $this->crud->hasAccessOrFail('create');
        $this->crud->setOperation('create');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // insert item in the db
        $item = $this->crud->create($request->except(['save_action', '_token', '_method', 'current_tab', 'http_referrer']));
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->setSaveAction();

        $itemId = $item->getKey();

        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ?: \Request::input('id');

        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = $this->crud->route.'/create';
                break;
            case 'save_and_edit':
                $redirectUrl = $this->crud->route.'/'.$itemId.'/edit';
                if (\Request::has('current_tab')) {
                    $redirectUrl = $redirectUrl.'#'.\Request::get('current_tab');
                }
                if (\Request::has('locale')) {
                    $redirectUrl .= '?locale='.\Request::input('locale');
                }

                break;
            case 'save_and_back':
                $redirectUrl = route('crud.article.index');
                break;
            default:
                $redirectUrl = \Request::has('http_referrer') ? \Request::get('http_referrer') : $this->crud->route;
                break;
        }

        // if the request is AJAX, return a JSON response
        if ($this->request->ajax()) {
            return [
                'success' => true,
                'data' => $this->crud->entry,
                'redirect_url' => $redirectUrl,
            ];
        }

        return \Redirect::to($redirectUrl);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }

}
