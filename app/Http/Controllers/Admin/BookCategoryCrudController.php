<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookCategoryRequest as StoreRequest;
use App\Http\Requests\BookCategoryRequest as UpdateRequest;

/**
 * Class BookCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BookCategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\BookCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/book_category');
        $this->crud->setEntityNameStrings('danh mục sách', 'tất cả danh mục sách');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'loai_sach',
            'label' => 'Loại sách',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'dang_cau_hoi',
            'label' => 'Dạng câu hỏi',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'dang_cau_tra_loi',
            'label' => 'Dạng câu trả lời',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'country',
            'label' => 'Mã quốc gia',
            'type' => 'text',
        ]);

        // ------ CRUD FIELDS

        $this->crud->addField([    // TEXT
            'name' => 'loai_sach',
            'label' => 'Loại sách',
            'type' => 'text',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'dang_cau_hoi',
            'label' => 'Dạng câu hỏi',
            'type' => 'text',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'dang_cau_tra_loi',
            'label' => 'Dạng câu trả lời:',
            'type' => 'text',
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Quốc gia',
            'type' => 'select2',
            'name' => 'country',
            'entity' => 'country',
            'attribute' => 'name',
            'model' => "App\Models\Country",
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
