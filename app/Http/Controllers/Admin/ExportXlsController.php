<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Excel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Controllers\Controller;
use App\Models\Barcode;

class ExportXlsController extends Controller implements FromCollection, WithHeadings
{
    use Exportable;
    public $data_ex = [];
    public function __construct($data)
    {
        $this->data_ex = $data;
    }

    public function collection()
    {
        // return Barcode::all();
        $order = [];
        $data = $this->data_ex;
        foreach ($data as $row) {
            $order[] = array(
                '0' => $row['ma'],
                '1' => number_format($row['soluong']),
            );
        }

        return (collect($order));
    }
    public function headings(): array
    {
        return [
            'Ma_Tai_Lieu',
            'So_Luong',
        ];
    }
    // public function export(){
    //     return Excel::download(new ExcelController(), 'in_ma.xls');
    // }
}
