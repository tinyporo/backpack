<?php

namespace App\Http\Controllers\Admin;

use App\Models\AllPost;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AllPostRequest as StoreRequest;
use App\Http\Requests\AllPostRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class AllPostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AllPostCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\AllPost');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/all_post');
        $this->crud->setEntityNameStrings('dữ liệu', 'Dữ liệu tổng');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'hoi_dap_id',
            'label' => 'Unique ID',
        ]);
        $this->crud->addColumn([
            'name' => 'giaingay_link',
            'label' => 'Link trên giaingay',
            'type'          => "model_function_custom",
            'function_name' => 'getGiaingayLink',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'label' => 'Loại',
            'type' => 'text',
        ]);

        $this->crud->addColumn([
            'name' => 'level',
            'label' => 'Level',
            'type'  => "model_function_custom",
            'function_name' => 'upLevel',
        ]);

        $this->crud->addColumn([
            'name' => 'moderate_up_time',
            'label' => 'Th.gian lên level trung bình:',
            'type'          => "model_function_custom",
            'function_name' => 'getAverageUpTime',
        ]);

        $this->crud->addField([
            'label' => "Tags",
            'name' => 'tags',
            'type' => "tagsinput",
        ]);

        $this->crud->addFilter([
            'name' => 'level',
            'type' => 'text',
            'label'=>'Tìm kiếm theo level'
        ],false,function($value){
            $this->crud->addClause('where','level','LIKE', "%$value%");
        });

        $this->crud->addFilter([
            'name' => 'tags',
            'type' => 'text',
            'label'=>'Tìm kiếm theo tag'
        ],false,function($value){
            $this->crud->addClause('whereHas', 'taggs', function($query) use($value){
                $query->where('name','LIKE', "%$value%");
            });


//            ) ('where','taggs','LIKE', "%$value%");
        });

        $this->crud->enableAjaxTable();

        $user_logged = Auth::user();
        if($user_logged->hasRole('Administrator')){
            $this->crud->removeButton('create');
            $this->crud->removeButton('delete');
        }else{
            $this->crud->removeAllButtons();

        }

    }

    public function create()
    {
        return redirect()->back();
    }

    public function edit($id)
    {
        $user_logged = Auth::user();
        if(!$user_logged->hasRole('Administrator')) return redirect()->back();

        return parent::edit($id);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $id = $request->id;

        $post = AllPost::find($id);

        if(!$post){
            \Alert::error('Id item không đúng!')->flash();

            return redirect()->route('crud.all_post.index');
        }

        $tags = $request->tags;
        $tags = explode(',', $tags);
        $tags = array_filter($tags);

        if(count($tags) === 0){
            \Alert::info('Item không được gán tag nào cả!')->flash();

            return redirect()->route('crud.all_post.index');
        }

        try{
            $post->attachTags($tags);
            \Alert::success('Item đã được gán tag thành công!')->flash();
        }catch (\Exception $exception){
            \Alert::error($exception->getMessage())->flash();
        }

        return redirect()->route('crud.all_post.index');
    }

    public function upLevel($id, $level = null){
        $user_logged = Auth::user();

        if(!$user_logged){
            \Alert::info('Bạn cần đăng nhập lại để tiếp tục!')->flash();

            return redirect(route('backpack.auth.login'));
        }

        $post = AllPost::find($id);

        if(!$post){
            \Alert::error('Id item không đúng!')->flash();

            return redirect()->route('crud.all_post.index');
        }

        if($post->hasMaxLevel()){
            \Alert::info('Item đã đạt level tối đa!')->flash();

            return redirect()->route('crud.all_post.index');
        }

        if($level && $post->level[1] > $level[1]){
            \Alert::error('Không thể hạ level item!')->flash();

            return redirect()->route('crud.all_post.index');
        }

//        Tạm bỏ phân quyền
//        if(!$user_logged->canUpItemLevel($post)){
//            \Alert::error('Bạn không có quyền up level cho item này!')->flash();
//
//            return redirect()->back();
//        }

        $level_up = $post->levelUp($level);

        if($level_up){
            \Alert::success('Item đã được lên level thành công!')->flash();
        }else{
            \Alert::error('Có lỗi trong quá trình nâng level!')->flash();
        }

        return redirect()->route('crud.all_post.index');
    }

    public function showHistory($id){
        $post = AllPost::find($id);

        if(!$post){
            \Alert::error('Id item không đúng!')->flash();
            return redirect()->back();
        }

        $dataPoints = [];

        $dataPoints[] = [
            'x' =>  strtotime($post->created_at)*1000,
            'y' => 1
        ];

        $histories = \DB::table('level_up_histories')->where('hoi_dap_id', $post->hoi_dap_id)->get();


        foreach ($histories as $k => $history){
            $dataPoints[] = [
                'x' =>  strtotime($history->created_at)*1000,
                'y' => $k + 2
            ];
        }

        return view('show_history', ['hoi_dap_id' => $post->hoi_dap_id, 'dataPoints' => $dataPoints]);
    }
}
