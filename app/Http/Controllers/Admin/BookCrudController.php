<?php

namespace App\Http\Controllers\Admin;

use App\Models\Book;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BookRequest as StoreRequest;
use App\Http\Requests\BookRequest as UpdateRequest;

use Carbon\Carbon;

/**
 * Class BookCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BookCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Book');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/book');
        $this->crud->setEntityNameStrings('sách', 'tất cả sách');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Tên sách',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'isbn',
            'label' => 'ISBN',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'class',
            'label' => 'Lớp',
            'type'  => "select",
            'entity' => 'classes',
            'attribute' => 'name',
            'model' => "App\Models\Classes",
        ]);
        $this->crud->addColumn([
            'name' => 'category',
            'label' => 'Danh mục',
            'type'  => "select",
            'entity' => 'book_category',
            'attribute' => 'loai_sach',
            'model' => "App\Models\BookCategory",
        ]);
        $this->crud->addColumn([
            'name' => 'tap',
            'label' => 'Tập',
            'type' => 'number',
        ]);
        $this->crud->addColumn([
            'name' => 'chuyen',
            'label' => 'Chuyên',
            'type' => 'boolean',
        ]);
        $this->crud->addColumn([
            'name' => 'province',
            'label' => 'Tỉnh',
            'type'  => "select",
            'entity' => 'my_province',
            'attribute' => 'name',
            'model' => "App\Models\Province",
        ]);
        $this->crud->addColumn([
            'name' => 'publisher',
            'label' => 'Nhà xuất bản',
            'type'  => "select",
            'entity' => 'my_publisher',
            'attribute' => 'name',
            'model' => "App\Models\Publisher",
        ]);
        $this->crud->addColumn([
            'name' => 'pub_year',
            'label' => 'Năm xuất bản',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'republist',
            'label' => 'Tái xuất bản',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'author',
            'label' => 'Tác giả',
            'type'  => "select",
            'entity' => 'my_author',
            'attribute' => 'name',
            'model' => "App\Models\Author",
        ]);
        $this->crud->addColumn([
            'name' => 'language',
            'label' => 'Ngôn ngữ',
            'type'  => "select",
            'entity' => 'my_country',
            'attribute' => 'lang_name',
            'model' => "App\Models\Country",
        ]);
        $this->crud->addColumn([
            'name' => 'country',
            'label' => 'Tác giả',
            'type'  => "select",
            'entity' => 'my_country',
            'attribute' => 'name',
            'model' => "App\Models\Country",
        ]);
        $this->crud->addColumn([
            'name' => 'level',
            'label' => 'Level',
            'type'  => "select",
            'entity' => 'my_level',
            'attribute' => 'name',
            'model' => "App\Models\Level",
        ]);


        // ------ CRUD FIELDS
        $this->crud->addField([
            'name' => 'id',
            'label' => 'Mã sách',
            'type' => 'text',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Tên sách',
            'type' => 'text',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'isbn',
            'label' => 'ISBN',
            'type' => 'text',
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Lớp',
            'type' => 'select2',
            'name' => 'class',
            'entity' => 'classes',
            'attribute' => 'name',
            'model' => "App\Models\Classes",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Môn',
            'type' => 'select2',
            'name' => 'subject',
            'entity' => 'subject',
            'attribute' => 'name',
            'model' => "App\Models\Subject",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Danh mục',
            'type' => 'select2',
            'name' => 'category',
            'entity' => 'book_category',
            'attribute' => 'loai_sach',
            'model' => "App\Models\BookCategory",
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'tap',
            'label' => 'Tập',
            'type' => 'number',
            'default' => 0,
            'attributes' => ["min" => "0"]
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'chuyen',
            'label' => 'Chuyên',
            'type' => 'select_from_array',
            'options' => [0 => 'Chuyên', 1 => 'Không chuyên'],
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Tỉnh',
            'type' => 'select2',
            'name' => 'province',
            'entity' => 'my_province',
            'attribute' => 'name',
            'model' => "App\Models\Province",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Publisher',
            'type' => 'select2',
            'name' => 'publisher',
            'entity' => 'publisher',
            'attribute' => 'name',
            'model' => "App\Models\Publisher",
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'pub_year',
            'label' => 'Năm xuất bản',
            'type' => 'text',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'republist',
            'label' => 'Tái bản',
            'type' => 'text',
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Tác giả',
            'type' => 'select2',
            'name' => 'author',
            'entity' => 'author',
            'attribute' => 'name',
            'model' => "App\Models\Author",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Ngôn ngữ',
            'type' => 'select2',
            'name' => 'language',
            'entity' => 'country',
            'attribute' => 'lang_name',
            'model' => "App\Models\Country",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Quốc gia',
            'type' => 'select2',
            'name' => 'country',
            'entity' => 'country',
            'attribute' => 'name',
            'model' => "App\Models\Country",
        ]);
        $this->crud->addField([    // SELECT
            'label' => 'Level sách',
            'type' => 'select2',
            'name' => 'level',
            'entity' => 'level',
            'attribute' => 'name',
            'model' => "App\Models\Level",
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'status',
            'label' => 'Trạng thái',
            'type' => 'select_from_array',
            'options' => ['Đang trong kho' => 'Đang trong kho', 'Không có ở kho' => 'Không có ở kho'],
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'description',
            'label' => 'Mô tả',
            'type' => 'text',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'status',
            'label' => 'Trạng thái',
            'type' => 'select_from_array',
            'options' => [Book::IS_HARD_COPY => 'Bản cứng', Book::NOT_HARD_COPY  => 'Không phải bản cứng'],
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'digital_copy_url',
            'label' => 'Url tới bản mềm',
            'type' => 'text',
        ]);
        $this->crud->addField([
            'name' => 'inserted_by',
            'type' => 'text',
            'label' => '',
            'default' => \Auth::user()->name,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);
        $this->crud->addField([
            'name' => 'inserted_date',
            'type' => 'datetime',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);
        $this->crud->addField([
            'name' => 'modified_by',
            'type' => 'text',
            'label' => '',
            'default' => \Auth::user()->name,
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);
        $this->crud->addField([
            'name' => 'modified_date',
            'type' => 'datetime',
            'label' => '',
            'default' => Carbon::now()->toDateTimeString(),
            'attributes' => [
                'style' => 'display:none',
            ],
        ]);


        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
