<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PublisherRequest as StoreRequest;
use App\Http\Requests\PublisherRequest as UpdateRequest;

/**
 * Class PublisherCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PublisherCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Publisher');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/publisher');
        $this->crud->setEntityNameStrings('nhà xuất bản', 'các nhà xuất bản');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'id',
            'label' => 'ID',
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'label' => 'Tên nhà xuất bản',
            'type' => 'text',
        ]);
        $this->crud->addColumn([
            'name' => 'country',
            'label' => 'Mã quốc gia',
            'type' => 'text',
        ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'name',
            'label' => 'Tên nhà xuất bản',
            'type' => 'text',
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Quốc gia',
            'type' => 'select2',
            'name' => 'country',
            'entity' => 'country',
            'attribute' => 'name',
            'model' => "App\Models\Country",
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
