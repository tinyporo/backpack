<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ods;
use App\Models\AllPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileController
{
    private $files = [];

    public function indexSqlCoordinate()
    {
        return view('sql_coordinate');
    }

    public function indexSql()
    {
        return view('sql');
    }

    public function indexMediaFormat2()
    {
        return view('media_format2');
    }

    public function indexPdfZip()
    {
        return view('pdf_zip');
    }

    public function exportSqlCoordinate(Request $request){
        $ids = $request->get('ids');

        $ids = explode('-', $ids );
        $ids = array_filter( $ids );

        if ( count( $ids ) == 1 ){
            $ids = explode(',', $ids[0]);
            $ids = array_filter($ids, function($id){
                return is_numeric($id);
            });

            $posts = AllPost::whereIn('id', $ids)->get();
        }
        elseif ( count( $ids ) == 2 ){
            $min = intval($ids[0]);
            $max = intval($ids[1]);

            $posts = AllPost::where('id', '<=', $max)
                ->where('id', '>=', $min)->get();
        }else{
            $posts = [];
        }

        $file_path = storage_path('new.ods');
        $this->files[] = $file_path;

        $object = $this->newOds();
        $object->addCell(0,0,0,'id','string');
        $object->addCell(0,0,1,'tieu_de','string');
        $object->addCell(0,0,2,'url','string');
        $object->addCell(0,0,3,'duong_dan_hoi','string');
        $object->addCell(0,0,4,'duong_dan_tra_loi','string');

        foreach ($posts as $k => $post){
            $object->addCell(0, $k+1,0,$post->id,'int');
            $object->addCell(0,$k+1,1,$post->tieu_de,'string');
            $object->addCell(0,$k+1,2,$post->url,'string');
            $object->addCell(0,$k+1,3,$post->duong_dan_hoi,'string');
            $object->addCell(0,$k+1,4,$post->duong_dan_tra_loi,'string');
        }

        $this->saveOds($object,$file_path); //save the object to a ods file

        return Response::download($file_path, 'toa_do.ods');
    }

    private function newOds() {
        $content = '<?xml version="1.0" encoding="UTF-8"?>
	<office:document-content xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" office:version="1.0"><office:scripts/><office:font-face-decls><style:font-face style:name="Liberation Sans" svg:font-family="&apos;Liberation Sans&apos;" style:font-family-generic="swiss" style:font-pitch="variable"/><style:font-face style:name="DejaVu Sans" svg:font-family="&apos;DejaVu Sans&apos;" style:font-family-generic="system" style:font-pitch="variable"/></office:font-face-decls><office:automatic-styles><style:style style:name="co1" style:family="table-column"><style:table-column-properties fo:break-before="auto" style:column-width="2.267cm"/></style:style><style:style style:name="ro1" style:family="table-row"><style:table-row-properties style:row-height="0.453cm" fo:break-before="auto" style:use-optimal-row-height="true"/></style:style><style:style style:name="ta1" style:family="table" style:master-page-name="Default"><style:table-properties table:display="true" style:writing-mode="lr-tb"/></style:style></office:automatic-styles><office:body><office:spreadsheet><table:table table:name="Hoja1" table:style-name="ta1" table:print="false"><office:forms form:automatic-focus="false" form:apply-design-mode="false"/><table:table-column table:style-name="co1" table:default-cell-style-name="Default"/><table:table-row table:style-name="ro1"><table:table-cell/></table:table-row></table:table><table:table table:name="Hoja2" table:style-name="ta1" table:print="false"><table:table-column table:style-name="co1" table:default-cell-style-name="Default"/><table:table-row table:style-name="ro1"><table:table-cell/></table:table-row></table:table><table:table table:name="Hoja3" table:style-name="ta1" table:print="false"><table:table-column table:style-name="co1" table:default-cell-style-name="Default"/><table:table-row table:style-name="ro1"><table:table-cell/></table:table-row></table:table></office:spreadsheet></office:body></office:document-content>';
        $obj = new ods();
        $obj->parse($content);
        return $obj;
    }

    private function saveOds(ods $obj,$file) {
        $charset = ini_get('default_charset');
        ini_set('default_charset', 'UTF-8');
        $tmp = $this->get_tmp_dir();
        $uid = uniqid();
        mkdir($tmp.'/'.$uid);
        file_put_contents($tmp.'/'.$uid.'/content.xml',$obj->array2ods());
        file_put_contents($tmp.'/'.$uid.'/mimetype','application/vnd.oasis.opendocument.spreadsheet');
        file_put_contents($tmp.'/'.$uid.'/meta.xml',$obj->getMeta('es-ES'));
        file_put_contents($tmp.'/'.$uid.'/styles.xml',$obj->getStyle());
        file_put_contents($tmp.'/'.$uid.'/settings.xml',$obj->getSettings());
        mkdir($tmp.'/'.$uid.'/META-INF/');
        mkdir($tmp.'/'.$uid.'/Configurations2/');
        mkdir($tmp.'/'.$uid.'/Configurations2/acceleator/');
        mkdir($tmp.'/'.$uid.'/Configurations2/images/');
        mkdir($tmp.'/'.$uid.'/Configurations2/popupmenu/');
        mkdir($tmp.'/'.$uid.'/Configurations2/statusbar/');
        mkdir($tmp.'/'.$uid.'/Configurations2/floater/');
        mkdir($tmp.'/'.$uid.'/Configurations2/menubar/');
        mkdir($tmp.'/'.$uid.'/Configurations2/progressbar/');
        mkdir($tmp.'/'.$uid.'/Configurations2/toolbar/');
        file_put_contents($tmp.'/'.$uid.'/META-INF/manifest.xml',$obj->getManifest());
        shell_exec('cd '.$tmp.'/'.$uid.';zip -r '.escapeshellarg($file).' ./');
        ini_set('default_charset',$charset);
    }

    private function get_tmp_dir() {
        $path = '';
        if(!function_exists('sys_get_temp_dir')){
            $path = $this->try_get_temp_dir();
        }else{
            $path = sys_get_temp_dir();
            if(is_dir($path)){
                return $path;
            }else{
                $path = $this->try_get_temp_dir();
            }
        }
        return $path;
    }

    private function try_get_temp_dir() {
        // Try to get from environment variable
        if(!empty($_ENV['TMP'])){
            $path = realpath($_ENV['TMP']);
        }else if(!empty($_ENV['TMPDIR'])){
            $path = realpath( $_ENV['TMPDIR'] );
        }else if(!empty($_ENV['TEMP'])){
            $path = realpath($_ENV['TEMP']);
        }
        // Detect by creating a temporary file
        else{
            // Try to use system's temporary directory
            // as random name shouldn't exist
            $temp_file = tempnam(md5(uniqid(rand(),TRUE)),'');
            if ($temp_file){
                $temp_dir = realpath(dirname($temp_file));
                unlink($temp_file);
                $path = $temp_dir;
            }else{
                return "/tmp";
            }
        }
        return $path;
    }

    public function exportSql(Request $request){
        $ids = $request->get('ids');

        $ids = explode('-', $ids );
        $ids = array_filter( $ids );

        $csv_filename = 'hoi_dap_'.date('Y_m_d').'.sql';
        $csv_filepath = storage_path("sql/$csv_filename");
        $this->files[] = $csv_filepath;

        $mysqldump_path = config('data.mysqldump_path', 'mysqldump');
        $database_name = env('DB_DATABASE', 'topica_question_crawl');
        $username = env('DB_USERNAME', 'tuannp');
        $password = env('DB_PASSWORD', 'tiny1817');

        if ( count( $ids ) == 1 ){
            $ids = implode(',', $ids);
            $ids = explode( ",", $ids);
            $ids = array_filter($ids, function($id){
                return is_numeric($id);
            });
            $ids = implode(',', $ids);

            exec($mysqldump_path.' -u '.$username.' '.$database_name.' all_posts --password="'.$password.'" --where="id in ('.$ids.')" > '.$csv_filepath);

        }
        elseif ( count( $ids ) == 2 ){
            $min = intval($ids[0]);
            $max = intval($ids[1]);

            exec($mysqldump_path.' -u '.$username.' '.$database_name.' all_posts --password="'.$password.'" --where="id between '.$min.' and '.$max.'" > '.$csv_filepath);

        }
        return Response::download($csv_filepath, $csv_filename);
    }

    public function exportMediaFormat2(Request $request){
        $zip_file_path = storage_path('Media.zip');
        $this->files[] = $zip_file_path;

        if(file_exists($zip_file_path)) unlink($zip_file_path);

        $media_path = storage_path('Media');
        if(!is_dir($media_path)) @mkdir($media_path);
        @chmod($media_path, 0777);
        $problem_path = storage_path('Media/Problems');
        if(!is_dir($problem_path)) @mkdir($problem_path);
        @chmod($problem_path, 0777);
        $solution_path = storage_path('Media/Solutions');
        if(!is_dir($solution_path)) @mkdir($solution_path);
        @chmod($solution_path, 0777);

        $ids = $request->get('ids');

        $ids = explode('-', $ids );
        $ids = array_filter( $ids );


        if ( count( $ids ) == 1 ){
            $ids = explode( ",", $ids[ 0 ]);
            $ids = array_filter($ids, function($id){
                return is_numeric($id);
            });

            foreach($ids as $id){
                $this->extractImage( $id);
            }
        }
        elseif ( count( $ids ) == 2 ){

            for ($id = $ids[0]; $id < $ids[1]; $id++){
                $this->extractImage( $id);
            }

        }

        try{
            $rootPath = $media_path;

            $zip = new \ZipArchive();
            $zip->open($zip_file_path, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($rootPath),
                \RecursiveIteratorIterator::LEAVES_ONLY
            );

            foreach ($files as $name => $file)
            {
                if (!$file->isDir())
                {
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    $zip->addFile($filePath, $relativePath);
                }
            }

            $zip->close();
        }catch (\Exception $e){
            \Alert::error($e->getMessage())->flash();
        }finally{
            $this->deleteDir($media_path);
        }

        return Response::download($zip_file_path, 'Media.zip');
    }

    private function extractImage($id){
        $id = intval($id);
        $post = \DB::table('all_posts')->where('id', $id)->first();
        if(!$post) return;

        $hoi_dap_id = $post->hoi_dap_id;

        $src_dir = "/var/www/html/TestProject/storage/Media";
        $problem_folder = "$src_dir/Problems/problem_id_$hoi_dap_id";
        $solution_folder = "$src_dir/Solutions/solution_id_$hoi_dap_id";

        @chmod($problem_folder, 0777);
        @chmod($solution_folder, 0777);

        $tar_dir = "/var/www/html/backpack/storage/Media";
        $tar_problem_folder = "$tar_dir/Problems/problem_id_$hoi_dap_id";
        $tar_solution_folder = "$tar_dir/Solutions/solution_id_$hoi_dap_id";

        $this->recurse_copy($problem_folder, $tar_problem_folder);
        $this->recurse_copy($solution_folder, $tar_solution_folder);
    }

    private function recurse_copy($src,$dst) {
        if(!is_dir($src)) return;

        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    public function uploadPdfZip(Request $request){
        if($request->hasFile('fupload')){
            $file = $request->file('fupload');

            /** @var UploadedFile $file */
            if($file->getMimeType() != 'application/zip'){
                \Alert::error('Không đúng định dạng zip!')->flash();

                return redirect()->back();
            }

            $original_name = $file->getClientOriginalName();
            $real_path = $file->getRealPath();

            $storage_path = storage_path($original_name);

            $this->files[] = $storage_path;

            @chmod($real_path, 0777);
            $moved = @rename($real_path, $storage_path);

            if(!$moved){
                \Alert::error('Không thể chuyển file zip!')->flash();

                return redirect()->back();
            }

            $zip = new \ZipArchive();
            if ($zip->open($storage_path) === TRUE) {
                $zip->extractTo(env( 'UPLOAD_ORIGINAL_PDF_PATH', '/var/www/html/anh-pdf/').preg_replace('/\..*/', '', $original_name));
                $zip->close();

                \Alert::success('Update ảnh thành công!')->flash();

                return redirect()->back();
            } else {
                \Alert::error('Không thể mở file zip!')->flash();

                return redirect()->back();
            }
        }else{
            \Alert::error('Không nhận được file nén ảnh!')->flash();

            return redirect()->back();
        }
    }

    public static function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new \InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    public function __destruct()
    {
        foreach ($this->files as $file){
            if(file_exists($file)) unlink($file);
        }
    }
}
