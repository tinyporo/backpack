<?php
/**
 * Created by PhpStorm.
 * User: TinyPoro
 * Date: 12/17/18
 * Time: 10:26 AM
 */

namespace App\Helpers;


class Meta
{
     static $table_name = 'meta';

    public static function isExisted($key){
        $meta_data = \DB::table(self::$table_name)
            ->where('key', $key);

        return ($meta_data) ? $meta_data : false;
    }

    public static function setMeta($key, $value){
        if(self::isExisted($key)){
            \DB::table(self::$table_name)
                ->where('key', $key)
                ->update(['value' => $value]);
        }

        \DB::table(self::$table_name)
            ->insrt([
               'key' => $key,
               'value' => $value
            ]);
    }

    public static function getMeta($key){
        if($meta_data = self::isExisted($key)) return $meta_data->value;

        return null;
    }
}