<?php
/**
 * Created by PhpStorm.
 * User: TinyPoro
 * Date: 1/5/19
 * Time: 7:08 PM
 */

namespace App\Helpers;


class DateIntervalHelper
{
    public static function format($timestamp)
    {
        $string = '';

        $day = floor($timestamp/86400);
        $timestamp -= $day * 86400;
        $hour = floor($timestamp/3600);
        $timestamp -= $hour * 3600;
        $minute = floor($timestamp/60);
        $timestamp -= $minute * 60;
        $second = floor($timestamp);

        $string = "$day ngày $hour giờ $minute phút $second giây";

        return $string;
    }

}