<?php

namespace App;

use App\Models\AllPost;
use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\User
 *
 */

class User extends Authenticatable
{
    use Notifiable;
    use CrudTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook_id', 'google_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function activity_logs(){
        return $this->hasMany('App\Models\BookStoreActivityLog', 'keeping_person', 'name');
    }

    public function book_store(){
        return $this->hasMany('App\Models\BookStore', 'keeping_person', 'name');
    }

    public function canUpItemLevel(AllPost $post){
        if($post->hasMaxLevel()) return false;

        if($this->hasRole('Administrator')) return true;

        $level_number = intval($post->level[1]);

        for($i = $level_number + 1; $i <= 8; $i++){
            $check_permission = "L$i";

            if($this->can($check_permission)) return true;
        }

        return false;

    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
