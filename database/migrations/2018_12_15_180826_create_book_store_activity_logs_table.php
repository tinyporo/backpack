<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookStoreActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_store_activity_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id');
            $table->integer('store')->nullable();
            $table->string('activity', 255);
            $table->string('keeping_person', 50)->nullable();

            $table->string('inserted_by', 150);
            $table->dateTime('inserted_date');
            $table->string('modified_by', 150);
            $table->dateTime('modified_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_store_activity_logs');
    }
}
