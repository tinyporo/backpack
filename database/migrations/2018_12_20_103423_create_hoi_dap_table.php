<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoiDapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoi_dap', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hoi_dap_id', 50)->unique();
            $table->string('book_code');
            $table->text('de_bai');
            $table->text('dap_an');
            $table->string('duong_dan_hoi')->nullable();
            $table->string('duong_dan_tra_loi')->nullable();
            $table->integer('created_by');
            $table->integer('is_handled')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoi_dap');
    }
}
