<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookcodeprinttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookcodeprinttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bookcodeprinttypes_name', 150);
            $table->string('bookcodeprinttypes_code', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookcodeprinttypes');
    }
}
