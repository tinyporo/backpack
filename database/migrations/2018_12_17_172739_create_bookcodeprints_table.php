<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookcodeprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookcodeprints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bookcodeprint_book_id', 150);
            $table->string('bookcodeprint_inserted_by', 150);
            $table->string('bookcodeprint_modified_by', 150);
            $table->string('bookcodeprint_country', 150);
            $table->string('bookcodeprint_type', 10);
            $table->tinyInteger('bookcodeprint_status')->default(0);
            $table->dateTime('bookcodeprint_inserted_date');
            $table->dateTime('bookcodeprint_modified_date');
            // $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookcodeprints');
    }
}
