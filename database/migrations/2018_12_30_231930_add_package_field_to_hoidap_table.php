<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPackageFieldToHoidapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hoi_dap', function (Blueprint $table) {
            $table->text('file_path')->nullable();
            $table->string('nguoi_nhap_lieu', 50)->nullable();
            $table->string('packageid', 16)->nullable();
            $table->string('nguon_du_lieu', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hoi_dap', function (Blueprint $table) {
            $table->dropColumn('package');
        });
    }
}
