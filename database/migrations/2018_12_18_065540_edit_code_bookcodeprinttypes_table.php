<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditCodeBookcodeprinttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookcodeprinttypes', function (Blueprint $table) {
            $table->string('bookcodeprinttypes_code', 10)->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookcodeprinttypes', function (Blueprint $table) {
            $table->dropColumn('bookcodeprinttypes_code');
        });
    }
}
