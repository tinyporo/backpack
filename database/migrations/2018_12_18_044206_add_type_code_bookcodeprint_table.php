<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeCodeBookcodeprintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookcodeprints', function (Blueprint $table) {
            $table->string('bookcodeprints_type_code', 150);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookcodeprints', function (Blueprint $table) {
            $table->dropColumn('bookcodeprints_type_code');
        });
    }
}
