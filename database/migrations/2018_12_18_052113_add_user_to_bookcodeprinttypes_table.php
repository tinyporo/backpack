<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserToBookcodeprinttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookcodeprinttypes', function (Blueprint $table) {
            $table->string('bookcodeprinttypes_inserted_by', 150);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookcodeprinttypes', function (Blueprint $table) {
            $table->dropColumn('bookcodeprinttypes_inserted_by');
        });
    }
}
