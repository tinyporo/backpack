<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HandledDataHoidapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hoi_dap', function (Blueprint $table) {
            $table->text('de_bai_full')->nullable();
            $table->text('dap_an_full')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hoi_dap', function (Blueprint $table) {
            $table->dropColumn('de_bai_full');
            $table->dropColumn('dap_an_full');
        });
    }
}
