<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditForeignCodeBookcodeprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookcodeprints', function (Blueprint $table) {
            $table->foreign('bookcodeprint_type')->references('bookcodeprinttypes_code')->on('bookcodeprinttypes')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookcodeprints', function (Blueprint $table) {
            //
        });
    }
}
