<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->string('id', 16)->primary();
            $table->string('name')->nullable();
            $table->string('isbn', 20)->nullable();
            $table->integer('class')->nullable();
            $table->integer('subject')->nullable();
            $table->integer('category')->nullable();
            $table->integer('tap')->nullable();
            $table->boolean('chuyen')->nullable();
            $table->integer('province')->nullable();
            $table->integer('publisher')->nullable();
            $table->string('pub_year', 50)->nullable();
            $table->string('republish', 50)->nullable();
            $table->integer('author')->nullable();
            $table->string('language', 10)->nullable();
            $table->string('country', 10)->nullable();
            $table->string('level', 2)->nullable();
            $table->string('status', 50)->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_hard_copy')->nullable();
            $table->string('digital_copy_url', 50)->nullable();

            $table->string('inserted_by', 150);
            $table->dateTime('inserted_date');
            $table->string('modified_by', 150);
            $table->dateTime('modified_date');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
