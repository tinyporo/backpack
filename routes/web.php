<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});


Auth::routes();

Route::get('/home', function () {
    return redirect('/admin');
})->name('home');

Route::get('/cau-hoi/{id}', [
    'as'   => 'frontend.post.detail',
    'uses' => 'Frontend\ArticleController@detail'
]);

/** SOCIALIZE LOGIN */
Route::group(['prefix' => 'app'], function () {
    Route::get('{provider}/login','SocialiteAuthController@redirectToProvider')->name('social.login');;
    Route::get('{provider}/callback', 'SocialiteAuthController@handleProviderCallback');
});
