<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('article', 'ArticleCrudController');
    CRUD::resource('hoi_dap', 'HoiDapCrudController');
//    CRUD::resource('raw_data', 'RawDataCrudController');
//    CRUD::resource('raw_type', 'RawTypeCrudController');
//    CRUD::resource('question_answer', 'QuestionAnswerCrudController');

    CRUD::resource('pdf_book', 'PdfBookCrudController');

    CRUD::resource('book', 'BookCrudController');
    CRUD::resource('book_store_activity_log', 'BookStoreActivityLogCrudController');
    CRUD::resource('book_store', 'BookStoreCrudController');
    CRUD::resource('classes', 'ClassesCrudController');
    CRUD::resource('subject', 'SubjectCrudController');
    CRUD::resource('book_category', 'BookCategoryCrudController');
    CRUD::resource('book_sub_category', 'BookSubCategoryCrudController');
    CRUD::resource('province', 'ProvinceCrudController');
    CRUD::resource('country', 'CountryCrudController');
    CRUD::resource('publisher', 'PublisherCrudController');
    CRUD::resource('author', 'AuthorCrudController');
    CRUD::resource('user', 'UserCrudController');
    CRUD::resource('level', 'LevelCrudController');

    Route::get('all_post/up_level/{id}/{level?}',  'AllPostCrudController@upLevel')->name('allpost_uplevel');
    Route::get('all_post/show_history/{id}',  'AllPostCrudController@showHistory')->name('show_histories');
    CRUD::resource('all_post', 'AllPostCrudController');

    //xuất sql
    Route::get('/export_sql', 'FileController@indexSql')->name('get.sql');
    Route::post('/export_sql', 'FileController@exportSql')->name('export.sql');

    //xuất tọa độ
    Route::get('/export_sql_coordinate', 'FileController@indexSqlCoordinate')->name('get.sql.coordinate');
    Route::post('/export_sql_coordinate', 'FileController@exportSqlCoordinate')->name('export.sql.coordinate');

    //xuất media format 2
    Route::get('/export_media_format2', 'FileController@indexMediaFormat2')->name('get.media.format2');
    Route::post('/export_media_format2', 'FileController@exportMediaFormat2')->name('export.media.format2');


    //upload file nén ảnh pdf gốc
    Route::get('/upload_pdf_zip', 'FileController@indexPdfZip')->name('get.upload.pdf_zip');
    Route::post('/upload_pdf_zip', 'FileController@uploadPdfZip')->name('upload.pdf_zip');

    CRUD::resource('book-code-print', 'BookCodePrintCrudController');
    CRUD::resource('file-type', 'BookCodePrintTypesCrudController');
}); // this should be the absolute last line of this file