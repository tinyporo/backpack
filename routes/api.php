<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('item/create', [
    'as' => 'api.item.create',
    'uses' => 'Api\V1\HoidapController@create'
]);

Route::post('item/update', [
    'as' => 'api.item.update',
    'uses' => 'Api\V1\HoidapController@update'
]);

Route::get('item/exists', [
    'as' => 'api.item.exist',
    'uses' => 'Api\V1\HoidapController@exist'
]);

Route::post('book/create', [
    'as' => 'api.pdf_book.create',
    'uses' => 'Api\V1\PdfBookController@create'
]);

Route::get('book/exists', [
    'as' => 'api.pdf_book.exist',
    'uses' => 'Api\V1\PdfBookController@exist'
]);
